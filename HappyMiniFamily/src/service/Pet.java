package service;

import java.util.Arrays;
public class Pet implements PetService {
    String species;
    String nickname;
    int age;
    String[] habits;
    int trickLevel;

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {
    }

    public Pet(String species, String nickname, int age, String[] habits, int trickLevel) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.habits = habits;
        this.trickLevel = trickLevel;
    }

    @Override
    public void eat() {
        System.out.println("I'm eating");
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am - " + nickname + ". I miss you!");
    }

    @Override
    public void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    public String toString() {
        return species+"{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}
