package service;

public interface PetService {
    void eat();
    void respond();
    void foul();
}
