package service;

import java.util.Arrays;
import java.util.Random;

public class Human implements HumanService {
    String name;
    String surname;
    int year;
    int IQ;
    Pet pet;
    Human father;
    Human mother;
    String[][] schedule = new String[7][2];

    @Override
    public void greetPet() {
        System.out.println("Hello, " + pet.nickname);
    }

    @Override
    public void describePet() {
        String slyCheck = pet.trickLevel > 50 ? "very sly" : "almost not sly";
        System.out.println("I have " + pet.species + ",he is " + pet.age + " years old," + "and he " + slyCheck);
    }

    @Override
    public void feedPet(boolean isItTimeForFeeding) {
        Random random = new Random();
        int generatedNumber = random.nextInt(100) + 1;
        if (isItTimeForFeeding || generatedNumber < pet.trickLevel) {
            System.out.println("Hm... I will feed " + pet.nickname);
        } else {
            System.out.println("I think " + pet.nickname + " is not hungry.");
        }
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human() {

    }

    public Human(String name, String surname, int year, Human father, Human mother) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.father = father;
        this.mother = mother;
    }

    public Human(String name, String surname, int year, int IQ, Pet pet, Human father, Human mother, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.IQ = IQ;
        this.pet = pet;
        this.father = father;
        this.mother = mother;
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", IQ=" + IQ +
                ", father=" + father.name + " " + father.surname +
                ", mother=" + mother.name + " " + mother.surname +
                ", pet=" + pet.species + "{nickname=" + pet.nickname +
                ", age=" + pet.age + ", trickLevel=" + pet.trickLevel +
                ", habits=" + Arrays.toString(pet.habits) +
                '}';
    }

    public String[][] enterSchedule() {
        schedule[0][0] = "sunday";
        schedule[1][0] = "monday";
        schedule[2][0] = "tuesday";
        schedule[3][0] = "wednesday";
        schedule[4][0] = "thursday";
        schedule[5][0] = "friday";
        schedule[6][0] = "saturday";
        schedule[0][1] = "go to cinema";
        schedule[1][1] = "go to school";
        schedule[2][1] = "do homework";
        schedule[3][1] = "go to carting";
        schedule[4][1] = "go out with friends";
        schedule[5][1] = "go to dinner with family";
        schedule[6][1] = "go to driving school";
        return schedule;
    }

}
