package service;

public interface HumanService {
    void greetPet();
    void describePet();
    void feedPet(boolean isItTimeForFeeding);
}
