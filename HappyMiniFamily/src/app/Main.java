package app;

import service.Human;
import service.Pet;

public class Main {
    public static void main(String[] args) {
        Human human = new Human();
        Human mike = new Human("Mike", "Doe", 1940);
        Human valeria = new Human("Valeria", "Doe", 1945);
        Human billy = new Human("Billy", "Jonas", 1890);
        Human kelly = new Human("Kelly", "Jonas", 1900);
        Pet sugar = new Pet("cat", "Sugar");
        Human john = new Human("John", "Doe", 1978, 90, sugar, mike, valeria, human.enterSchedule());
        Pet pet = new Pet();
        Human caroline = new Human("Caroline", "Doe", 1980, 86, sugar, billy, kelly, human.enterSchedule());
        Pet rambo = new Pet("Dog", "Rambo", 2, new String[]{"eat", "drink", "sleep"}, 70);
        Human michael = new Human("Michael", "Doe", 2005, 89, rambo, john, caroline, human.enterSchedule());
        Human barry = new Human("Barry", "Doe", 2006, john, caroline);
        System.out.println(michael);
        System.out.println(rambo);
        System.out.println(sugar);
        System.out.println(john);
        System.out.println(caroline);
        michael.describePet();
        michael.greetPet();
        michael.feedPet(true);
        michael.feedPet(false);
        rambo.eat();
        rambo.respond();
        rambo.foul();
    }
}
