import org.junit.jupiter.api.Test;
import service.DomesticCat;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTest {
    DomesticCat sugar = new DomesticCat("Sugar", 2, 77, "eat", "sleep", "myau");

    @Test
    public void eatTest() {
        String expected = "Sugar DOMESTICCAT is eating";
        assertEquals(expected, sugar.eat());
    }

    @Test
    public void respondTest() {
        String expected = "Myauu";
        assertEquals(expected, sugar.respond());
    }

    @Test
    public void foulTest() {
        String expected = "I need cleaning";
        assertEquals(expected, sugar.foul());
    }
}
