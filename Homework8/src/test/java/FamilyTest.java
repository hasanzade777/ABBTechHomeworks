import org.junit.jupiter.api.Test;
import service.DomesticCat;
import service.Family;
import service.Human;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {
    Human human = new Human();
    Human caroline = new Human("Caroline", "Doe", 1955, 76, human.enterSchedule());
    Human john = new Human("John", "Doe", 1944, 77, human.enterSchedule());
    Human michael = new Human("Michael", "Doe", 1995, 85, human.enterSchedule());
    Human dolares = new Human("Dolares", "Doe", 1996, 84, human.enterSchedule());
    Human volki = new Human("Volki", "Dolki", 1988);
    DomesticCat sugar = new DomesticCat("Sugar", 2, 77, "eat", "sleep", "myau");
    Family familyOfJohn = new Family(caroline, john, Set.of(sugar), dolares, michael, volki);

    @Test
    public void countFamilyTest() {
        int expected = 6;
        assertEquals(expected, familyOfJohn.countFamily());
    }

    @Test
    public void deleteFamilyChildIndexTest() {
        boolean assertIndexTrue = familyOfJohn.deleteChild(0);
        boolean assertIndexFalse = familyOfJohn.deleteChild(10);
        assertTrue(assertIndexTrue);
        assertFalse(assertIndexFalse);
    }

    @Test
    public void deleteFamilyChildObjectTest() {
        Human huuman = new Human("Yuri", "Kalayev", 1999);
        boolean assertIndexTrue = familyOfJohn.deleteChild(dolares);
        boolean assertIndexFalse = familyOfJohn.deleteChild(huuman);
        assertTrue(assertIndexTrue);
        assertFalse(assertIndexFalse);
    }

    @Test
    public void familyEqualsTest() {
        Family family = new Family(caroline, john, Set.of(sugar), dolares, michael, volki);
        Family family2 = new Family(dolares, john, Set.of(sugar), volki);
        boolean assertEqualsTrue = family.equals(familyOfJohn);
        boolean assertEqualsFalse = family2.equals(familyOfJohn);
        assertTrue(assertEqualsTrue);
        assertFalse(assertEqualsFalse);
    }

    @Test
    public void familyHashcodeTest() {
        Family family = new Family(caroline, john, Set.of(sugar), dolares, michael, volki);
        Family family2 = new Family(dolares, john, Set.of(sugar), volki);
        boolean assertEqualsTrue = family.hashCode() == familyOfJohn.hashCode();
        boolean assertEqualsFalse = family2.hashCode() == familyOfJohn.hashCode();
        assertTrue(assertEqualsTrue);
        assertFalse(assertEqualsFalse);
    }

}
