package service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class Family {
    private Human mother;
    private Human father;
    private ArrayList<Human> children;
    private Set<Pet> pet;

    public Family() {
    }

    public Family(Human mother, Human father, Set<Pet> pet, Human... children) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.children = (ArrayList<Human>) Arrays.stream(children).collect(Collectors.toList());
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        children.add(child);
    }

    public boolean deleteChild(int index) {
        if (index < children.size()) {
            children.remove(index);
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human child) {
        for (Human delete : children) {
            if (delete.equals(child)) {
                children.remove(delete);
                return true;
            }
        }
        return false;
    }

    public int countFamily() {
        int familyMembers = 0;
        if (father != null) familyMembers++;
        if (mother != null) familyMembers++;
        if (pet != null) familyMembers++;
        for (Human human : children) {
            if (human != null) familyMembers++;
        }
        return familyMembers;
    }

    @Override
    public String toString() {
        return father.getSurname() + " Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!mother.equals(family.mother)) return false;
        return father.equals(family.father);
    }

    @Override
    public int hashCode() {
        int result = father.hashCode() - 30000000;
        result = 5 * result + mother.hashCode();
        return result;
    }

    public void familySetter(Family family, Human father, Human mother, ArrayList<Human> children, Set<Pet> pet) {
        family.setFather(father);
        family.setMother(mother);
        family.setChildren(children);
        family.setPet(pet);
    }

    @Override
    protected void finalize() {
        System.out.println("The Finalize of Family called");
    }
}

