package service;

import java.util.Set;

public class Sheep extends Pet {
    @Override
    public String respond() {
        return ("I'm sheep mee");
    }

    public Sheep() {
    }

    public Sheep(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, Set.of(habits));
        this.setSpecies(Species.UNKNOWN);
    }
}
