package service;

import java.util.Set;

public class DomesticCat extends Pet implements FoulService {
    public DomesticCat() {
    }

    public DomesticCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, Set.of(habits));
        this.setSpecies(Species.DOMESTICCAT);
    }

    @Override
    public String respond() {
        return ("Myauu");
    }

    @Override
    public String foul() {
        return ("I need cleaning");
    }
}
