package service;

import java.util.Set;

public class RoboCat extends Pet {
    public RoboCat() {
    }

    public RoboCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, Set.of(habits));
        this.setSpecies(Species.ROBOCAT);
    }

    @Override
    public String respond() {
        return ("Bip-Bip");
    }

}
