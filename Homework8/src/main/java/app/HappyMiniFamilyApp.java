package app;

import service.*;

import java.util.Set;

public class HappyMiniFamilyApp {

    public static void main(String[] args) {
        Human human = new Human();
        Dog dog = new Dog("Rex", 12, 88, "Eat", "Grr", "Walk");
        Fish fish = new Fish("Bloop", 1, 11, "Swim", "Nothing much");
        DomesticCat domesticCat = new DomesticCat("Mesi", 5, 99, "Eat", "Play", "Joy");
        RoboCat roboCat = new RoboCat("Robot", 3, 0, "Fast", "Charging", "");
        Man John = new Man("John", "Doe", 1987,102,human.enterSchedule());
        Woman Sue = new Woman("Sue", "Doe", 1998);
        Sheep sheep = new Sheep("Qoyun", 3, 12, "eat", "give milk");
        Family familyOfJohn = new Family(Sue, John, Set.of(domesticCat));
        John.setFamily(familyOfJohn);
        Sue.setFamily(familyOfJohn);

        System.out.println(John);
    }
}
