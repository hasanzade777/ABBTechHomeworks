package service;


import java.util.Scanner;

public class WeekPlannerServiceImpl implements WeekPlannerService {
    private final String[][] schedule = new String[7][2];
    private final Scanner scanner = new Scanner(System.in);
    private String weekDay = "";
    private boolean isExitNotCalled = true;

    @Override
    public void scheduleWeek() {
        enterWeeksAndSchedules();
        while (isExitNotCalled) {
            System.out.print("Please, input day of week: ");
            weekDay = checkExit(weekDay);
            if (weekDay.startsWith("change") || weekDay.startsWith("reschedule")) changeSchedule();
            else {
                switch (weekDay) {
                    case "sunday":
                        printSchedule();
                        break;
                    case "monday":
                        printSchedule();
                        break;
                    case "tuesday":
                        printSchedule();
                        break;
                    case "wednesday":
                        printSchedule();
                        break;
                    case "thursday":
                        printSchedule();
                        break;
                    case "friday":
                        printSchedule();
                        break;
                    case "saturday":
                        printSchedule();
                        break;
                    case "exit":
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Sorry, I don't understand you, please try again.");
                        break;
                }
            }
        }
    }

    private void printSchedule() {
        for (String[] strings : schedule) {
            for (int j = 0; j < schedule.length - 5; j++)
                if (strings[j].equals(weekDay)) {
                    System.out.println("Your tasks for " +
                            strings[j] + " " + strings[j + 1]);
                    break;
                }
        }
    }

    private void enterWeeksAndSchedules() {
        schedule[0][0] = "sunday";
        schedule[1][0] = "monday";
        schedule[2][0] = "tuesday";
        schedule[3][0] = "wednesday";
        schedule[4][0] = "thursday";
        schedule[5][0] = "friday";
        schedule[6][0] = "saturday";
        schedule[0][1] = "go to cinema";
        schedule[1][1] = "go to school";
        schedule[2][1] = "do homework";
        schedule[3][1] = "go to carting";
        schedule[4][1] = "go out with friends";
        schedule[5][1] = "go to dinner with family";
        schedule[6][1] = "go to driving school";
    }

    private String checkExit(String weekDay) {

        if (weekDay.equals("exit")) {
            isExitNotCalled = false;
        } else {
            weekDay=scanner.nextLine().toLowerCase();
        }
        weekDay=weekDay.replaceAll(" ","");
        return weekDay;
    }

    private void changeSchedule() {
        String change;
        for (String[] strings : schedule) {
            for (int j = 0; j < schedule.length - 5; j++)
                if (weekDay.endsWith(strings[j])) {
                    System.out.println("Please, input new tasks for " + strings[j]);
                    change = scanner.nextLine();
                    strings[j + 1] = change;
                }
        }
    }
}
