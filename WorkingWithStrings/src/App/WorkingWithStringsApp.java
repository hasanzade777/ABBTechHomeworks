package app;

import service.WeekPlannerService;
import service.WeekPlannerServiceImpl;


public class WorkingWithStringsApp {
    public static void main(String[] args) {
        WeekPlannerService weekPlannerService = new WeekPlannerServiceImpl();
        weekPlannerService.scheduleWeek();
    }
}
