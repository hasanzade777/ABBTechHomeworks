package Lesson7;

import java.util.Scanner;

public class BoxInBox {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter first box width,height,length");
        Box firstBox = new Box(sc.nextInt(), sc.nextInt(), sc.nextInt());
        System.out.println("Please enter second box width,height,length");
        Box secondBox = new Box(sc.nextInt(), sc.nextInt(), sc.nextInt());
        System.out.println(compareBoxes(firstBox,secondBox));
    }

    public static String compareBoxes(Box firstBox, Box secondBox) {
        if(firstBox.getHeight() < secondBox.getHeight()) {
            if(firstBox.getWidth() * firstBox.getLength() <= secondBox.getLength() * secondBox.getWidth()) {
                return "YES";
            }
        }
        return "NO";
    }
}
