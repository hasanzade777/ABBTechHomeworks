package Lesson15;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CombineObjects {
    public static void main(String[] args) {
        List<String> subjects = List.of("Noel ", "The cat ", "The dog ");
        List<String> verbs = List.of("wrote ", "chased ", "slept on ");
        List<String> objects = List.of("the book ", "the ball ", "the bed ");
        List<String> sentences = allPossibleSentences(subjects, verbs, objects);
        System.out.println(sentences);
    }

    private static List<String> allPossibleSentences(List<String> subjects, List<String> verbs, List<String> objects) {
        List<String> sentences = new ArrayList<>();
        for (String sentence : subjects) {

            for (String verb : verbs) {
                verb = sentence.concat(verb);
                for (String object : objects) {
                    sentences.add(verb.concat(object));
                }
            }
        }
//        return subjects.stream().flatMap(
//                s -> s.concat(verbs.stream().flatMap(
//                        v-> objects.stream().map(o->
//                                new Sentence(s,v,o)
//                        )))).collect(Collectors.toList());
        return new ArrayList<>();
    }
}
