package Lesson9;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArrayFilters {

    public static void main(String[] args) {
        List<Integer> numbersList= Arrays.asList(1,3,4,-5,-6,-7,4,34,5,6,3,-3,4,5,-6,7);
        String conditionPostive = ">" ;
        String conditionNegative = "<" ;
        //System.out.println(getPositiveNumbers(numbersList));
       // System.out.println(getNegativeNumbers(numbersList));
//        System.out.println(getNegativeAndPositive(numbersList,conditionPostive));
//        System.out.println(getNegativeAndPositive(numbersList,conditionNegative));

    }

    private static ArrayList<Integer> getPositiveNumbers(List<Integer> numbersList) {
        ArrayList<Integer> list = new ArrayList<>();
        for(int i:numbersList){
            if(i > 0 ) list.add(i);
        }
        return list;
    }

    private static ArrayList<Integer> getNegativeNumbers(List<Integer> numbersList) {
        ArrayList<Integer> list = new ArrayList<>();
        for(int i:numbersList){
            if(i < 0 ) list.add(i);
        }
        return list;
    }
}
