package Brackets;

import java.util.*;

public class BracketsApp {
    private static final Map<Character,Character> pairs = Map.of('{','}','[',']','<','>','(',')');
    public static boolean isValid(String line) {
        Stack<Character> stack1 = new Stack<>();
        Stack<Character> stack2 = new Stack<>();
        Stack<Character> stack3 = new Stack<>();
        Stack<Character> stack4 = new Stack<>();
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '{') stack1.push(line.charAt(i));
            else if (line.charAt(i) == '(') stack2.push(line.charAt(i));
            else if (line.charAt(i) == '[') stack3.push(line.charAt(i));
            else if (line.charAt(i) == '<') stack4.push(line.charAt(i));
        }
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '}') {
                if (!stack1.isEmpty()) {
                    stack1.pop();
                } else return false;
            }
            if (line.charAt(i) == ')') {
                if (!stack2.isEmpty()) {
                    stack2.pop();
                } else return false;
            }
            if (line.charAt(i) == ']') {
                if (!stack3.isEmpty()) {
                    stack3.pop();
                } else return false;
            }
            if (line.charAt(i) == '>') {
                if (!stack4.isEmpty()) {
                    stack4.pop();
                } else return false;
            }
        }
        return stack1.isEmpty() && stack2.isEmpty() && stack3.isEmpty() && stack4.isEmpty();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        System.out.println(isValid(line));
        System.out.println(isValid2(line));
        Map<Character,Character> pairs = Map.of('{','}','[',']','<','>','(',')');
    }

    static boolean isValid2(String line) {
        List<Stack<Character>>stack1 = new ArrayList<>();
        return true;
    }
}
