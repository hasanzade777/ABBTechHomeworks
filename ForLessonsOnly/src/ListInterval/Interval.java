package ListInterval;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Interval {
    public static List<String> possibleMoves(String move) {
        Character character = move.charAt(0);//e
        int number = Integer.parseInt(String.valueOf(move.charAt(1)));//5
        List<Integer> numbers = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
        HashMap<Character, List<Integer>> allMoves = new HashMap<>(Map.of(
                'a', numbers,
                'b', numbers,
                'c', numbers,
                'd', numbers,
                'e', numbers,
                'f', numbers,
                'g', numbers,
                'h', numbers
        ));
        var ss = allMoves.entrySet().stream().filter(x ->
                x.getKey() == character).collect(Collectors.toList());
        ss.get(0).setValue(numbers);
        var ww = allMoves.entrySet().stream().filter(
                x -> x.getKey() != character
        ).collect(Collectors.toList());
        ww.forEach(x -> x.setValue(List.of(number)));
        System.out.println(ss);
        System.out.println(ww);
        List<String> moves = new ArrayList<>();
        String str = "%c%d";
        Optional<String> ox = Optional.empty();
        System.out.println(ox.orElse("33"));
        return moves;
    }

    public static void main(String[] args) {
        String str = "e5";
        System.out.println(possibleMoves(str));
    }

}
