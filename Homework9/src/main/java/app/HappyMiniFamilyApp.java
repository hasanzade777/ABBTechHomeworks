package app;

import model.DomesticCat;
import service.classes.CollectionFamilyDao;
import service.classes.FamilyController;

public class HappyMiniFamilyApp {
    public static void main(String[] args) {
        FamilyController controller = new FamilyController();
        System.out.println(controller.getFamilyById(1));
        System.out.println(controller.getAllFamilies());
        System.out.println(controller.displayAllFamilies());
        System.out.println(controller.deleteFamilyByIndex(0));
        System.out.println(controller.getPets(2));
        System.out.println(controller.count());
        System.out.println(controller.getFamiliesBiggerThan(2));
        System.out.println(controller.getFamiliesLessThan(4));
        controller.deleteAllChildenOlderThen(105);
        System.out.println(controller.displayAllFamilies());
        System.out.println(controller.adoptTheChild(controller.getFamilyById(0),
                controller.getFamilyById(0).getChildren().get(0)));
        System.out.println(controller.bornChild(controller.getFamilyById(1),"feminine"));
        controller.addPet(0, new DomesticCat("Cat", 12, 72));
        System.out.println(controller.getFamilyById(0));
    }
}
