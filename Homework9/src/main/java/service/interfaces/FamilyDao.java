package service.interfaces;

import model.Family;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(int i);

    boolean deleteFamily(int i);

    boolean deleteFamily(Family family);

    void saveFamily(Family family);

    List<Family> getFamiliesBiggerThan(int biggerThan);

    List<Family> getFamiliesLessThan(int lessThan);

    public Family getFamily(Family family);
}
