package service.interfaces;

public interface HumanIntrfc {
    String greetPet();

    String describePet();

    String feedPet(boolean isItTimeForFeeding);
}
