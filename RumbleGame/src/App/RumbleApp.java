package App;

import Service.RumbleService;
import Service.RumbleServiceImpl;

public class RumbleApp {
    public static void main(String[] args) {
        RumbleService rumbleService = new RumbleServiceImpl();
        rumbleService.startRumble();
        rumbleService.shootTarget();
    }
}
