package Service;

public interface RumbleService {
    void startRumble();
    void shootTarget();
}
