package Service;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

public class RumbleServiceImpl implements RumbleService {
    private final Scanner scanner = new Scanner(System.in);
    private final Random random = new Random();
    private final String[][] rumbleArray = new String[6][6];


    @Override
    public void startRumble() {
        System.out.println("All set. Get ready to rumble!");
        System.out.println();
        int first = selectTarget();
        int second = selectTarget();
        rumbleArray[first][second] = "x";
        for (int i = 0; i <= 5; i++) {
            for (int j = 0; j <= 5; j++) {
                if (!Objects.equals(rumbleArray[i][j], "x")) rumbleArray[i][j] = "-";
            }
        }

    }

    @Override
    public void shootTarget() {
        boolean isTargetNotShoot = true;
        while (isTargetNotShoot) {
            System.out.print("Please enter row from 1-5 : ");
            int row = scanner.nextInt();
            row = checkRange(row);
            System.out.print("\nPlease enter column 1-5 : ");
            int column = scanner.nextInt();
            column = checkRange(column);
            if (Objects.equals(rumbleArray[row][column], "x")) {
                System.out.println("You have won ! ");
                for (int i = 0; i <= 5; i++) {
                    System.out.print(i);
                    for (int j = 0; j <= 5; j++) {
                        if (j == 0) continue;
                        if (i == 0) System.out.printf("| %d |", j);
                        else {
                            System.out.printf("| %s |", rumbleArray[i][j]);
                        }
                    }
                    System.out.println();
                }
                isTargetNotShoot = false;
            } else {
                rumbleArray[row][column] = "*";
                for (int i = 0; i <= 5; i++) {
                    System.out.print(i);
                    for (int j = 0; j <= 5; j++) {
                        if (j == 0) continue;
                        if (i == 0) System.out.printf("| %d |", j);
                        else {
                            if (Objects.equals(rumbleArray[i][j], "x"))
                                System.out.print("| - |");
                            else System.out.printf("| %s |", rumbleArray[i][j]);
                        }
                    }
                    System.out.println();
                }
            }

        }

    }

    private int checkRange(int i) {
        while (i > 5 || i <= 0) {
            System.out.println("Please enter number from 1-5");
            i = scanner.nextInt();
        }
        return i;
    }

    private int selectTarget() {
        return random.nextInt(5) + 1;
    }
}
