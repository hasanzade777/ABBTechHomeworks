package app;

import service.*;

public class HappyMiniFamilyApp {

    public static void main(String[] args) {
        Dog dog = new Dog("Rex", 12, 88, "Eat", "Grr", "Walk");
        Fish fish = new Fish("Bloop", 1, 11, "Swim", "Nothing much");
        DomesticCat domesticCat = new DomesticCat("Mesi", 5, 99, "Eat", "Play", "Joy");
        RoboCat roboCat = new RoboCat("Robot", 3, 0, "Fast", "Charging", "");
        Man John = new Man("John", "Doe", 1987);
        Woman Sue = new Woman("Sue", "Doe", 1998);
        Sheep sheep = new Sheep("Qoyun", 3, 12, "eat", "give milk");
        Family familyOfJohn = new Family(Sue, John, domesticCat);
        John.setFamily(familyOfJohn);
        Sue.setFamily(familyOfJohn);

        //Dog
        System.out.println(dog);
        dog.eat();
        dog.respond();
        dog.foul();
        System.out.println();
        //Fish
        System.out.println(fish);
        fish.eat();
        fish.respond();
        System.out.println();
        //DomesticCat
        System.out.println(domesticCat);
        domesticCat.eat();
        domesticCat.respond();
        domesticCat.foul();
        System.out.println();
        //roboCat
        System.out.println(roboCat);
        roboCat.eat();
        roboCat.respond();
        //Woman
        Sue.greetPet();
        Sue.describePet();
        Sue.feedPet(true);
        Sue.makeUp();
        //Man
        John.greetPet();
        John.describePet();
        John.feedPet(false);
        John.repairCar();
        //Sheep
        System.out.println(sheep);
        sheep.respond();
        sheep.eat();
    }
}
