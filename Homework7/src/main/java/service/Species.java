package service;

public enum Species {
    DOMESTICCAT,
    DOG,
    ROBOCAT,
    FISH,
    UNKNOWN;

}