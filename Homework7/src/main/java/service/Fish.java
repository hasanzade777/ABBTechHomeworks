package service;

public class Fish extends Pet {
    public Fish() {
    }

    public Fish(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.FISH);
    }

    @Override
    public String respond() {
        return "";
    }
}
