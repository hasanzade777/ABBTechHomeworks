package service;

public class RoboCat extends Pet {
    public RoboCat() {
    }

    public RoboCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.ROBOCAT);
    }

    @Override
    public String respond() {
        return ("Bip-Bip");
    }

}
