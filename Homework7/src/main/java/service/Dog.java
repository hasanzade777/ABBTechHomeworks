package service;

public class Dog extends Pet implements FoulService {
    public Dog() {

    }

    public Dog(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, habits);
        this.setSpecies(Species.DOG);
    }

    @Override
    public String respond() {
        return ("hav-hav");
    }

    @Override
    public String foul() {
        return ("Please wash me");
    }
}
