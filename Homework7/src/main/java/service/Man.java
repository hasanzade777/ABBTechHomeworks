package service;

public class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int IQ, String[][] schedule) {
        super(name, surname, year, IQ, schedule);
    }

    public Man(String name, String surname, int year, int IQ, String[][] schedule, Family family) {
        super(name, surname, year, IQ, schedule, family);
    }

    @Override
    public String describePet() {
        return (super.getFamily().getPet().getSpecies() + " "
                + super.getFamily().getPet().getNickname() + " my pet is strong!");
    }

    public void repairCar() {
        System.out.println(super.getName() + " I'm repairing car");
    }
}
