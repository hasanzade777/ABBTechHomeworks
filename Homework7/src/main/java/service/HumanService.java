package service;

public interface HumanService {
    String greetPet();

    String describePet();

    String feedPet(boolean isItTimeForFeeding);
}
