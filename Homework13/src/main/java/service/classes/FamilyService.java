package service.classes;

import model.*;
import service.interfaces.FamilyDao;
import service.interfaces.FileSaveService;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FamilyService {
    private final FileSaveService fileSave = new FileSave();
    private final FamilyDao familyDao = new CollectionFamilyDao();

    public List<String> getAllFamilies() {
        return familyDao.getAllFamilies().stream().map(x -> x.getFather().getSurname().concat(" Family")).collect(Collectors.toList());
    }

    public List<Family> displayAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int biggerThan) {
        return familyDao.getFamiliesBiggerThan(biggerThan);
    }

    public List<Family> getFamiliesLessThan(int lesserThan) {
        return familyDao.getFamiliesLessThan(lesserThan);
    }

    public Integer countFamiliesWithMemberNumber(Family family) {
        return family.countFamily();
    }

    public void createNewFamily(Human father, Human mother) {
        familyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int familyIndex) {
        return familyDao.deleteFamily(familyIndex);
    }

    public Family bornChild(Family family, String type) {
        if (Objects.equals(type, "masculine")) {
            Random random = new Random();
            List<String> maleNames = List.of("Joshua", "Kenneth", "Brian", "George", "Ronald");
            var name = maleNames.get(random.nextInt(5));
            Man baby = new Man(name, family.getFather().getSurname(), LocalDate.now());
            family.addChild(baby);
            familyDao.saveFamily(family);
            return family;
        } else if (Objects.equals(type, "feminine")) {
            Random random = new Random();
            List<String> femaleNames = List.of("Elizabeth", "Eleanor", "Zoey", "Stella", "Victoria");
            var name = femaleNames.get(random.nextInt(5));
            Woman baby = new Woman(name, family.getFather().getSurname(), LocalDate.now());
            family.addChild(baby);
            familyDao.saveFamily(family);
            return family;
        }
        return null;
    }

    public Family adoptTheChild(Family family, Human child) {
        try {
            var fam = familyDao.getFamily(family);
            fam.deleteChild(child);
            familyDao.saveFamily(fam);
            return familyDao.getFamily(fam);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("No Family");
            return null;
        }
    }

    public void deleteAllChildrenOlderThen(int olderThen) {
        int thisYear = LocalDate.now().getYear();
        Predicate<Human> predicate = (x) -> thisYear - x.getBirthDate().getYear() > olderThen;
        familyDao.getAllFamilies().stream().filter(family -> family.getChildren() != null).forEach(childFamily -> childFamily.getChildren().removeIf(predicate));
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int familyIndex) {
        try {
            return familyDao.getFamilyByIndex(familyIndex);
        } catch (Exception e) {
            System.out.println("No Family");
            return null;
        }
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyDao.getFamilyByIndex(familyIndex).getPet();
    }

    public void addPet(int familyIndex, Pet pet) {
        var family = familyDao.getFamilyByIndex(familyIndex);
        HashSet<Pet> familyPets = new HashSet<>(family.getPet());
        familyPets.add(pet);
        family.setPet(familyPets);
        familyDao.saveFamily(family);
    }

    public void saveToDatabase(int FamilyID) {
        var fam = familyDao.getFamilyByIndex(FamilyID);
        fileSave.saveToFile(fam);
    }

    public void fromDatabaseGetAll() {
        try {
            Human mother = new Human();
            Human father = new Human();
            ArrayList<Human> children = new ArrayList<>();
            List<Human> humans = new ArrayList<>();
            Set<Pet> pets = new HashSet<>();
            List<Family> fam = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader("Database.txt"));
            var list = bufferedReader.lines().filter(l -> !l.isBlank()).collect(Collectors.toList());
            for (String lines : list) {
                if (Character.isUpperCase(lines.substring(1, 2).charAt(0))) {
                    pets.add(familyDao.fromDatabaseGetPets(lines));
                } else humans.add(familyDao.fromDatabaseGetHuman(lines));
            }
            for (int i = 0; i < humans.size(); i++){
                if(i==0) mother = humans.get(i);
                if(i==1) father = humans.get(i);
                else {
                    children.add(humans.get(i));
                }
            }
            Family family = new Family(mother,father);
            family.setChildren(children);
            family.setPet(pets);
            fam.add(family);
            familyDao.saveListFamily(fam);
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException thrown");
        }
    }
}
