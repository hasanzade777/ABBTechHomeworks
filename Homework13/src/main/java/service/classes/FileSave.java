package service.classes;

import model.Family;
import service.interfaces.FileSaveService;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileSave implements FileSaveService {
    public FileSave() {

    }

    @Override
    public void saveToFile(Family family) {
        try {
            FileWriter fileWriter = new FileWriter("Database.txt",true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(family.getMother().toString2());
            bufferedWriter.newLine();
            bufferedWriter.write(family.getFather().toString2());
            bufferedWriter.newLine();
            if (family.getChildren() != null) {
                family.getChildren().forEach(child -> {
                    try {
                        bufferedWriter.write(child.toString2());
                        bufferedWriter.newLine();
                    } catch (IOException e) {
                        System.out.println("Child write to database had exception");
                    }
                });
            }
            if (family.getPet() != null) {
                family.getPet().forEach(pet -> {
                    try {
                        bufferedWriter.write(pet.toString2());
                        bufferedWriter.newLine();
                    } catch (IOException e) {
                        System.out.println("Pets write to database had exception");
                    }
                });
            }
            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();
            System.out.println("Save to database successfully completed");
        } catch (Exception e) {
            System.out.println("Exception has thrown file not saved properly");
        }
    }
}
