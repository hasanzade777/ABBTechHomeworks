package service.interfaces;

import model.Family;

import java.io.FileNotFoundException;
import java.util.List;

public interface FileSaveService {
    void saveToFile(Family family);
}
