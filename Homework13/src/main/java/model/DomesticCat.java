package model;

import service.interfaces.FoulIntrfc;

import java.util.Set;

public class DomesticCat extends Pet implements FoulIntrfc {
    public DomesticCat() {
    }

    public DomesticCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, trickLevel, Set.of(habits), age);
        this.setSpecies(Species.DOMESTICCAT);
    }

    @Override
    public String foul() {
        return ("I need cleaning");
    }
}
