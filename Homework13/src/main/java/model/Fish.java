package model;

import java.util.Set;

public class Fish extends Pet {
    public Fish() {
    }

    public Fish(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, trickLevel, Set.of(habits), age);
        this.setSpecies(Species.FISH);
    }
}
