package model;

import java.util.Set;

public class Pet {
    private String nickname;
    private int age;
    private Set<String> habits;
    private int trickLevel;
    private Species species;

    public Pet() {
    }

    public Pet(String nickname, int trickLevel, Set<String> habits, int age) {
        this.nickname = nickname;
        this.age = age;
        this.habits = habits;
        this.trickLevel = trickLevel;
//        Predicate<Species> predicate = anyMatch -> getClass().getSimpleName().toUpperCase().equals(anyMatch.name());
//        this.species = Arrays.stream(Species.values()).anyMatch(predicate) ?
//         Species.valueOf(getClass().getSimpleName().toUpperCase()) : Species.UNKNOWN;
    }
    public Pet(Species species , String nickname,int age , int trickLevel){
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String eat() {
        return (this.nickname + " " + this.species + " is eating");
    }


    @Override
    public String toString() {
        return "Species=" + species +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel;
    }
    public String toString2() {
        return "" + species +
                ", " + nickname +
                ", " + age +
                ", " + trickLevel;
    }
}
