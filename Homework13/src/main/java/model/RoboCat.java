package model;

import java.util.Set;

public class RoboCat extends Pet {
    public RoboCat() {
    }

    public RoboCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, trickLevel, Set.of(habits), age);
    }
}
