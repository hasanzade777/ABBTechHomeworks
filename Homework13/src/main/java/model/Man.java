package model;

import java.time.LocalDate;
import java.util.Map;

public class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, LocalDate year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, LocalDate year, int IQ, Map<String, String> schedule) {
        super(name, surname, year, IQ, schedule);
    }

    public Man(String name, String surname, LocalDate year, int IQ, Map<String, String> schedule, Family family) {
        super(name, surname, year, IQ, schedule, family);
    }

    public Man(String name, String surname, LocalDate birthDate, int IQ) {
        super(name, surname, birthDate, IQ);
    }

    @Override
    public String describePet() {
        return (super.getFamily().getPet().iterator().next().getSpecies() + " "
                + super.getFamily().getPet().iterator().next().getSpecies() + " my pet is strong!");
    }

    public void repairCar() {
        System.out.println(super.getName() + " I'm repairing car");
    }
}
