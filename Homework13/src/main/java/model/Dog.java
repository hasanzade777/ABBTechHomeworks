package model;

import service.interfaces.FoulIntrfc;

import java.util.Set;

public class Dog extends Pet implements FoulIntrfc {
    public Dog() {

    }

    public Dog(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, trickLevel, Set.of(habits), age);
        this.setSpecies(Species.DOG);
    }

    @Override
    public String foul() {
        return ("Please wash me");
    }
}
