package model;

import java.util.*;
import java.util.stream.Collectors;

public class Family {
    private Human mother;
    private Human father;
    private ArrayList<Human> children;
    private Set<Pet> pet = new HashSet<>();

    public Family() {
    }

    public Family(Human mother, Human father, Set<Pet> pet, Human... children) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        this.pet = pet;
        this.children = (ArrayList<Human>) Arrays.stream(children).collect(Collectors.toList());
        this.children.forEach(x -> x.setFamily(this));
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public void addChild(Human child) {
        children.add(child);
    }

    public boolean deleteChild(int index) {
        if (index < children.size()) {
            children.remove(index);
            return true;
        }
        return false;
    }

    public boolean deleteChild(Human child) {
        for (Human delete : children) {
            if (delete.equals(child)) {
                children.remove(delete);
                return true;
            }
        }
        return false;
    }

    public int countFamily() {
        int familyMembers = 0;
        if (father != null) familyMembers++;
        if (mother != null) familyMembers++;
        if (pet != null) familyMembers++;
        if (children != null)
            for (Human ignored : children) {
                familyMembers++;
            }
        return familyMembers;
    }

    @Override
    public String toString() {
        return father.getName() + " Family:" +
                "\n" +
                "mother=" + mother +
                "\n" +
                "father=" + father +
                "\n" +
                "children=" + children +
                "\n" +
                "pet=" + pet;
    }

    public void prettyFormat() {
        System.out.println(father.getSurname() + " Family:" +
                "\n" +
                "   father: " + father +
                "\n" +
                "   mother: " + mother +
                "\n" +
                "   children:");
        if (Objects.nonNull(children)) for (Human human : children) System.out.printf("%s", human.prettyFormat());
        System.out.printf("pets: " + pet.toString());
        System.out.println();
        System.out.println();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!mother.equals(family.mother)) return false;
        return father.equals(family.father);
    }

    @Override
    public int hashCode() {
        int result = father.hashCode() - 30000000;
        result = 5 * result + mother.hashCode();
        return result;
    }

    public void familySetter(Family family, Human father, Human mother, ArrayList<Human> children, Set<Pet> pet) {
        family.setFather(father);
        family.setMother(mother);
        family.setChildren(children);
        family.setPet(pet);
    }

    @Override
    protected void finalize() {
        System.out.println("The Finalize of Family called");
    }
}

