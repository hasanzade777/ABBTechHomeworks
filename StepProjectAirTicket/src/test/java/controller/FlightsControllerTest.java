package controller;

import model.Bookings;
import model.Flights;
import model.Passengers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import service.impl.BookingsServiceImpl;
import service.impl.FlightsServiceImpl;
import servicesDAO.impl.BookingsDaoServiceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class FlightsControllerTest {
    @InjectMocks
    FlightsController flightsController = new FlightsController();
    @Mock
    BookingsServiceImpl bookingsService = new BookingsServiceImpl();
    @Mock
    FlightsServiceImpl flightsService = new FlightsServiceImpl();
    @Mock
    Flights flights;
    @Mock
    BookingsDaoServiceImpl bookingsDaoService = new BookingsDaoServiceImpl();
    @Mock
    Bookings bookings;
    @Mock
    List<Flights> flightsList = new ArrayList<>();
    @Mock
    List<Bookings> bookingsList = new ArrayList<>();

    @BeforeEach
    void setup() {
        flights = new Flights(1, "UKR2121", "KIEV", "BAKU", LocalDateTime.of(2022, 2, 11, 8, 30), 100);
        flightsList = List.of(flights);
        bookings = new Bookings(1, new Passengers("EE", "EE"), "UKR2121");
        bookingsList = List.of(bookings);
        flightsService = spy(flightsService);
        flightsController = spy(flightsController);
        bookingsService = spy(bookingsService);
        bookingsDaoService = spy(bookingsDaoService);
    }

    @Test
    void testShowOnlineBoardPrettyFormat() {
        doNothing().when(flightsService).showOnlineBoardPrettyFormat();
        flightsController.showOnlineBoardPrettyFormat();
        verify(flightsController).showOnlineBoardPrettyFormat();
    }

    @Test
    void testShowOneFlight() {
        when(flightsService.showOneFlight(anyString())).thenReturn(flights);
        when(flightsController.showOneFlight(anyString())).thenReturn(flights);
        Assertions.assertEquals(flights, flightsController.showOneFlight(anyString()));
    }

    @Test
    void testSearchTheFlight() {
        when(flightsService.getAllFlights()).thenReturn(new ArrayList<>());
        when(bookingsService.searchTheFlight("BAKU", LocalDateTime.of(2022, 2, 11, 8, 30), 1)).thenReturn(false);
        Assertions.assertFalse(flightsController.searchTheFlight("DEST", "22/02/2021 08:30", 1));
    }

    @Test
    void testSearchTheFlight2() {
        when(flightsService.getAllFlights()).thenReturn(List.of(flights));
        when(bookingsService.searchTheFlight("BAKU", LocalDateTime.of(2022, 2, 11, 8, 30), 1)).thenReturn(true);
        when(flightsController.searchTheFlight("BAKU","11/02/2022 08:30",1)).thenReturn(true);
        Assertions.assertTrue(flightsController.searchTheFlight("BAKU", "11/02/2022 08:30", 1));
    }

    @Test
    void testBookFlight() {
        doNothing().when(bookingsService).bookTheFlight("UKR", "EE", "EE");
        flightsController.bookTheFlight("UKR", "EE", "EE");
        verify(flightsController).bookTheFlight("UKR", "EE", "EE");
    }

    @Test
    void testCancelBookFlight() {
        when(bookingsDaoService.getBookingsForID(1)).thenReturn(bookings);
        when(bookingsService.cancelTheBooking(1)).thenReturn(true);
        when(flightsController.cancelTheBooking(1)).thenReturn(true);
        Assertions.assertTrue(flightsController.cancelTheBooking(1));
    }

    @Test
    void testMyFlights() {
        when(bookingsService.myFlights("EE", "EE")).thenReturn(new ArrayList<>());
        Assertions.assertEquals(Collections.emptyList(), flightsController.myFlights("ee", "ee"));
    }

    @Test
    void testLoadData() {
        doNothing().when(bookingsService).loadDataToApp();
        doNothing().when(flightsService).loadDataToApp();
        flightsController.loadData();
        verify(flightsController).loadData();
    }

    @Test
    void testAddData() {
        doNothing().when(bookingsService).saveAllBookings();
        doNothing().when(flightsService).saveAllFlights();
        flightsController.addAllDataToFile();
        verify(flightsController).addAllDataToFile();
    }
}

