package service.impl;

import model.Bookings;
import model.Flights;
import model.Passengers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import service.FlightsService;
import servicesDAO.BookingsDAOService;
import servicesDAO.impl.BookingsDaoServiceImpl;
import servicesDAO.impl.FileServiceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class BookingsServiceImplTest {
    @InjectMocks
    BookingsServiceImpl bookingsService = new BookingsServiceImpl();
    @Mock
    BookingsDaoServiceImpl bookingsDaoService;
    @Mock
    FlightsServiceImpl flightsService;
    @Mock
    FileServiceImpl fileService;
    List<Flights> flightsList;
    Flights flights;
    Bookings bookings;
    List<Bookings> bookingsList;

    @BeforeEach
    void setup() {
        bookingsService = spy(bookingsService);
        flightsService = spy(flightsService);
        bookingsDaoService = spy(bookingsDaoService);
        fileService = spy(fileService);
        flights = new Flights(2, "UKR11", "BAKU", "KIEV", LocalDateTime.of(1, 1, 1, 1, 1), 100);
        flightsList = List.of(flights);
        bookings = new Bookings(2, new Passengers("EE", "EE"), "UKR");
        bookingsList = List.of(bookings);
    }

    @Test
    void testSearchTheFlight() {
        assertFalse(bookingsService.searchTheFlight("Destination", LocalDateTime.of(1, 1, 1, 1, 1), 3));
    }

    @Test
    void testSearchFlight2() {
        when(flightsService.getAllFlights()).thenReturn(flightsList);
        when(bookingsService.searchTheFlight("KIEV", LocalDateTime.of(1,1,1,1,1),1)).thenReturn(true);
        assertTrue(bookingsService.searchTheFlight("KIEV", LocalDateTime.of(1, 1, 1, 1, 1), 1));
    }

    @Test
    void testBookFlight() {
        doNothing().when(flightsService).addBookingToFlight(new Bookings());
        bookingsService.bookTheFlight("UKR", "EE", "EE");
        verify(bookingsService).bookTheFlight("UKR", "EE", "EE");
    }

    @Test
    void testMyFlights() {
        when(bookingsDaoService.getAllBookings()).thenReturn(new ArrayList<>());
        assertEquals(Collections.emptyList(), bookingsService.myFlights("EE", "EE"));
    }

    @Test
    void testBookTheFlight() {
        doNothing().when(bookingsDaoService).addBooking(new Bookings());
        doNothing().when(flightsService).addBookingToFlight(new Bookings());
        bookingsService.bookTheFlight("KK", "KK", "KK");
        verify(bookingsService).bookTheFlight("KK", "KK", "KK");
    }

    @Test
    void testLoadDataToApp() {
        doNothing().when(fileService).loadDataBookings();
        bookingsService.loadDataToApp();
        verify(bookingsService).loadDataToApp();
    }
    @Test
    void testSave() {
        when(bookingsDaoService.getAllBookings()).thenReturn(bookingsList);
        doNothing().when(fileService).saveBookings(bookingsList);
        bookingsService.saveAllBookings();
        verify(bookingsService).saveAllBookings();
    }
    @Test
    void testCancelBooking(){
        when(bookingsDaoService.getAllBookings()).thenReturn(new ArrayList<>());
        assertFalse(bookingsService.cancelTheBooking(5));
    }

    @Test
    void testCancelBooking2(){
        when(bookingsDaoService.getAllBookings()).thenReturn(bookingsList);
        when(bookingsDaoService.getBookingsForID(2)).thenReturn(bookings);
        doNothing().when(flightsService).removeBooking(bookings);
        doNothing().when(bookingsDaoService).removeBooking(2);
        when(bookingsService.cancelTheBooking(2)).thenReturn(true);
        assertTrue(bookingsService.cancelTheBooking(2));
    }
    @Test
    void testMyFlights2() {
        when(bookingsDaoService.getAllBookings()).thenReturn(bookingsList);
        assertEquals(bookingsList, bookingsService.myFlights("EE", "EE"));
    }
}

