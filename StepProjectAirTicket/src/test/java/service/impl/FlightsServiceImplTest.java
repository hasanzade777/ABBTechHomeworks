package service.impl;

import model.Bookings;
import model.Flights;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import servicesDAO.impl.FileServiceImpl;
import servicesDAO.impl.FlightsDaoServiceImpl;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class FlightsServiceImplTest {
    @InjectMocks
    FlightsServiceImpl flightsService = new FlightsServiceImpl();
    @Mock
    FlightsDaoServiceImpl flightsDaoService =new FlightsDaoServiceImpl();
    @Mock
    FileServiceImpl fileService = new FileServiceImpl();
    Flights flights;
    List<Flights> flightsList;

    @BeforeEach
    void setup() {
        flightsService = spy(flightsService);
        flightsDaoService = spy(flightsDaoService);
        fileService = spy(fileService);
        flights = new Flights(2, "UKR11", "BAKU", "KIEV", LocalDateTime.of(2022, 1, 1, 12, 12), 100);
        flightsList = List.of(flights);
    }

    @Test
    void testGetAllFlights() {
        when(flightsDaoService.getAllFlights()).thenReturn(flightsList);
        when(flightsService.getAllFlights()).thenReturn(flightsList);
        assertEquals(flightsList, flightsService.getAllFlights());
    }

    @Test
    void testShowOnlineBoardPrettyFormat() {
        when(flightsService.getAllFlights()).thenReturn(flightsList);
        flightsService.showOnlineBoardPrettyFormat();
        verify(flightsService).showOnlineBoardPrettyFormat();
    }

    @Test
    void testShowOneFlight() {
        when(flightsDaoService.getBySerialNumberID("UKR11")).thenReturn(flights);
        when(flightsService.showOneFlight("UKR11")).thenReturn(flights);
        assertEquals(flights, flightsService.showOneFlight("UKR11"));
    }

    @Test
    void testLoadDataToApp() {
        doNothing().when(fileService).loadDataFlights();
        flightsService.loadDataToApp();
        verify(flightsService).loadDataToApp();
    }

    @Test
    void testAddBookingToFlight() {
        doNothing().when(flightsDaoService).addBookingToFlight(new Bookings());
        flightsService.addBookingToFlight(new Bookings());
        verify(flightsService).addBookingToFlight(new Bookings());
    }

    @Test
    void testRemoveBooking() {
        doNothing().when(flightsDaoService).removeBookingFromFlight(new Bookings());
        flightsService.removeBooking(new Bookings());
        verify(flightsService).removeBooking(new Bookings());
    }

    @Test
    void testSaveAllFlights() {
        when(flightsService.getAllFlights()).thenReturn(flightsList);
        doNothing().when(fileService).saveFlights(flightsList);
        flightsService.saveAllFlights();
        verify(flightsService).saveAllFlights();
    }
}
