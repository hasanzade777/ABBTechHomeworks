package servicesDAO.impl;

import model.Bookings;
import model.Flights;
import model.Passengers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import servicesDAO.BookingsDAOService;
import servicesDAO.FlightsDAOService;

import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class FileServiceImplTest {
    @Mock
    FlightsDAOService flightsDAOService;
    @Mock
    BookingsDAOService bookingsDAOService;
    @InjectMocks
    FileServiceImpl fileService = new FileServiceImpl();
    Flights flights;
    List<Flights> flightsList;
    Bookings bookings;
    List<Bookings> bookingsList;

    @BeforeEach
    void setup() {
        flightsDAOService = spy(flightsDAOService);
        bookingsDAOService = spy(bookingsDAOService);
        fileService = spy(fileService);
        flights = new Flights(1, "UKR", "KIEV", "BAKU", LocalDateTime.of(2022, 10, 10, 10, 10), 100);
        flightsList = List.of(flights);
        bookings = new Bookings(1, new Passengers("EE", "EE"), "UKR");
        bookingsList = List.of(bookings);
    }

    @Test
    void testLoadDataFlights() {
        doNothing().when(flightsDAOService).save(flightsList);
        fileService.loadDataFlights();
        verify(fileService).loadDataFlights();
    }

    @Test
    void testLoadDataBookings() {
        doNothing().when(bookingsDAOService).save(bookingsList);
        fileService.loadDataBookings();
        verify(fileService).loadDataBookings();
    }

    @Test
    void testSaveBookings() {
        fileService.saveBookings(bookingsList);
        verify(fileService).saveBookings(bookingsList);
    }

    @Test
    void testSaveFlights() {
        fileService.saveFlights(flightsList);
        verify(fileService).saveFlights(flightsList);
    }
}
