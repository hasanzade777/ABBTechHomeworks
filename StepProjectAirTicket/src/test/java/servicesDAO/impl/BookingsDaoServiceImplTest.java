package servicesDAO.impl;

import model.Bookings;
import model.Passengers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import servicesDAO.BookingsDAOService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class BookingsDaoServiceImplTest {
    @Mock
    BookingsDAOService bookingsDAOService = new BookingsDaoServiceImpl();
    Bookings bookings;
    List<Bookings> list;

    @BeforeEach
    void setup() {
        bookings = new Bookings(1, new Passengers("EE", "EE"), "UKR");
        list = List.of(bookings);
        bookingsDAOService = spy(bookingsDAOService);
    }

    @Test
    void testGetByNameSurname() {
        when(bookingsDAOService.getByNameSurname("EE", "EE")).thenReturn(bookings);
        assertEquals(bookings, bookingsDAOService.getByNameSurname("EE", "EE"));
    }

    @Test
    void testSave() {
        bookingsDAOService.save(list);
        verify(bookingsDAOService).save(list);
    }

    @Test
    void testAddBooking() {
        bookingsDAOService.addBooking(bookings);
        verify(bookingsDAOService).addBooking(bookings);
    }

    @Test
    void testRemoveBooking() {
        bookingsDAOService.removeBooking(1);
        verify(bookingsDAOService).removeBooking(1);
    }

    @Test
    void testGetAllBookings() {
        when(bookingsDAOService.getAllBookings()).thenReturn(list);
        assertEquals(list, bookingsDAOService.getAllBookings());
    }

    @Test
    void testGetBookingsForID() {
        when(bookingsDAOService.getBookingsForID(1)).thenReturn(bookings);
        assertEquals(bookings, bookingsDAOService.getBookingsForID(1));
    }

}
