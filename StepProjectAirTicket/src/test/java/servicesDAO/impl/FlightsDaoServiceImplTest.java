package servicesDAO.impl;

import model.Bookings;
import model.Flights;
import model.Passengers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class FlightsDaoServiceImplTest {
    @Mock
    FlightsDaoServiceImpl flightsDaoService = new FlightsDaoServiceImpl();
    Flights flights;
    List<Flights> list;
    Bookings bookings;

    @BeforeEach
    void setup() {
        flights = new Flights(1, "UKR", "UU", "R!!", LocalDateTime.of(2000, 11, 11, 11, 11), 100);
        list = List.of(flights);
        flightsDaoService = Mockito.spy(flightsDaoService);
        bookings = new Bookings(1, new Passengers("EE", "EE"), "UKR");
    }

    @Test
    void testGetAllFlights() {
        assertTrue((flightsDaoService.getAllFlights().isEmpty()));
    }

    @Test
    void testGetBySerialNumberID() {
        when(flightsDaoService.getBySerialNumberID("UKR")).thenReturn(flights);
        assertEquals(flights, flightsDaoService.getBySerialNumberID("UKR"));
    }

    @Test
    void testGetAllFlights2() {
        when(flightsDaoService.getAllFlights()).thenReturn(list);
        assertEquals(list, flightsDaoService.getAllFlights());
    }

    @Test
    void testSave() {
        doNothing().when(flightsDaoService).save(list);
        flightsDaoService.save(list);
        verify(flightsDaoService).save(list);
    }

    @Test
    void testAddBookingToFlight() {
        flightsDaoService.addBookingToFlight(bookings);
        verify(flightsDaoService).addBookingToFlight(bookings);
    }

    @Test
    void testRemoveBookingFromFlight() {
        flightsDaoService.removeBookingFromFlight(bookings);
        verify(flightsDaoService).removeBookingFromFlight(bookings);
    }
}

