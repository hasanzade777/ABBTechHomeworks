package model;

import java.time.LocalDateTime;
import java.util.List;
import static servicesDAO.impl.FileServiceImpl.dateTimeFormatter;

public class Flights {
    private int ID;
    private String flightSerialNumber;
    private String from;
    private String destination;
    private LocalDateTime flightDateTime;
    private int seats;
    private List<Bookings> bookings;

    @Override
    public String toString() {
        return ID +
                ", " + flightSerialNumber +
                ", " + from +
                ", " + destination +
                ", " + dateTimeFormatter.format(flightDateTime) +
                ", " + seats;
    }

    public String prettyFormat() {
        return "   " + flightSerialNumber +
                "              " + from +
                "    " + destination +
                "         " + dateTimeFormatter.format(flightDateTime) +
                "        " + seats;
    }

    public Flights(int ID, String flightSerialNumber, String from, String destination, LocalDateTime flightDateTime, int seats) {
        this.ID = ID;
        this.flightSerialNumber = flightSerialNumber;
        this.from = from;
        this.destination = destination;
        this.flightDateTime = flightDateTime;
        this.seats = seats;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFlightSerialNumber() {
        return flightSerialNumber;
    }

    public void setFlightSerialNumber(String flightSerialNumber) {
        this.flightSerialNumber = flightSerialNumber;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LocalDateTime getFlightDateTime() {
        return flightDateTime;
    }

    public void setFlightDateTime(LocalDateTime flightDateTime) {
        this.flightDateTime = flightDateTime;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public List<Bookings> getBookings() {
        return bookings;
    }

    public void setBookings(List<Bookings> bookings) {
        this.bookings = bookings;
    }

    public Flights() {
    }
}
