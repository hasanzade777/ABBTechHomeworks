package model;

import java.util.Objects;

public class Bookings {
    private int ID;

    private Passengers passenger;

    private String flightID;

    public Bookings() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bookings bookings = (Bookings) o;

        if (ID != bookings.ID) return false;
        if (!Objects.equals(flightID, bookings.flightID)) return false;
        return Objects.equals(passenger, bookings.passenger);
    }

    @Override
    public int hashCode() {
        int result = ID+105;
        return Objects.hashCode(result) ;
    }

    public Bookings(int ID, Passengers passenger, String flightID) {
        this.ID = ID;
        this.passenger = passenger;
        this.flightID = flightID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setPassenger(Passengers passenger) {
        this.passenger = passenger;
    }

    public void setFlightID(String flightID) {
        this.flightID = flightID;
    }

    public int getID() {
        return ID;
    }

    public Passengers getPassenger() {
        return passenger;
    }

    public String getFlightID() {
        return flightID;
    }

    @Override
    public String toString() {
        return  ID +
                ", " + passenger +
                ", " + flightID;
    }
}
