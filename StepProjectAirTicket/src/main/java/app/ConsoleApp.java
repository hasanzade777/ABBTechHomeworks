package app;

import controller.FlightsController;
import exception.FlightsExceptionClass;
import items.MenuItems;

import java.util.Scanner;
import model.Bookings;
import static servicesDAO.impl.FileServiceImpl.dateTimeFormatter;

public class ConsoleApp {
    public Scanner consoleInput = new Scanner(System.in);
    String inputForConsoleData;
    MenuItems menuItems = MenuItems.ITEMS;
    FlightsController flightsController = new FlightsController();

    public void run() {
        loadData();
        do {
            menuItems.showMenuItems();
            inputForConsoleData = consoleInput.next();
            answerToInputID(inputForConsoleData);
        } while (!inputForConsoleData.equals("6"));
    }

    private void answerToInputID(String inputForConsoleData) {
        if (!inputCheckStart(inputForConsoleData)) {
            new FlightsExceptionClass("Non existent Item");
        } else {
            int inputID = Integer.parseInt(inputForConsoleData);
            switch (inputID) {
                case 1: {
                    System.out.println("Flight Serial Number    From     To            Date and Time       Seats");
                    flightsController.showOnlineBoardPrettyFormat();
                    break;
                }
                case 2: {
                    System.out.println("Please enter SerialID of flight");
                    var flight = flightsController.showOneFlight(consoleInput.next());
                    if (flight.getID() == 0) {
                        System.out.println("There is no flight with this serial ID");
                    } else System.out.println(flight.prettyFormat());
                    break;
                }
                case 3: {
                    bookTheFlight();
                    break;
                }
                case 4: {
                    System.out.println("Please enter booking ID(number)");
                    var ID = consoleInput.next();
                    if (checkInteger(ID)) {
                        if (flightsController.cancelTheBooking(Integer.parseInt(ID))) {
                            System.out.println("Booking removed");
                        } else {
                            System.out.println("The ID is not right");
                        }
                        break;
                    } else {
                        System.out.println("The ID is not right");
                    }
                    break;
                }
                case 5: {
                    System.out.println("Please enter name");
                    var name = consoleInput.next();
                    System.out.println("Please enter surname");
                    var surname = consoleInput.next();
                    var bookings = flightsController.myFlights(name, surname);
                    for (Bookings booking : bookings) {
                        var flight = flightsController.showOneFlight(booking.getFlightID());
                        System.out.printf("%d %s %s %s %s %s %s \n",booking.getID(), booking.getPassenger().getName(),
                                booking.getPassenger().getSurname(), flight.getFlightSerialNumber(),
                                flight.getFrom(), flight.getDestination(), flight.getFlightDateTime().format(dateTimeFormatter));
                    }
                    break;
                }
                case 6: {
                    flightsController.addAllDataToFile();
                    System.out.println("Shutdown . . .");
                }
            }

        }
    }

    private void bookTheFlight() {
        try {
            System.out.println("Please enter destination,date,number of passengers");
            System.out.println("Destination: ");
            var destination = consoleInput.next();
            System.out.println("Date with format example 11/02/2022 08:30 : ");
            String inputDate = consoleInput.next();
            String inputTime = consoleInput.next();
            var date = inputDate + " " + inputTime;
            System.out.println("Passengers: ");
            var passengers = consoleInput.next();
            var isThereFlight = flightsController.searchTheFlight(destination, date, Integer.parseInt(passengers));
            while (isThereFlight) {
                System.out.println("Please enter SerialID of flight or go to main menu with type exit");
                var serialID = consoleInput.next();
                if (serialID.equals("exit")) break;
                if (checkFlight(serialID)) {
                    for (int i = 0; i < Integer.parseInt(passengers); i++) {
                        System.out.println("Please enter name: ");
                        var name = consoleInput.next();
                        System.out.println("Please enter surname: ");
                        var surname = consoleInput.next();
                        flightsController.bookTheFlight(serialID, name, surname);
                    }
                } else {
                    System.out.println("There is no flight with this serialNumber");
                }
            }
            if (!isThereFlight) System.out.println("There is no flight with your entered data");
        } catch (Exception e) {
            new FlightsExceptionClass(e.getMessage());
        }
    }

    private boolean checkFlight(String serialID) {
        var checker = flightsController.showOneFlight(serialID);
        return !checker.toString().isEmpty();
    }

    private boolean checkInteger(String console) {
        return console.matches("\\d+");
    }

    private boolean inputCheckStart(String inputForConsoleData) {
        return !inputForConsoleData.isBlank() && inputForConsoleData.matches("[1-9]") && inputForConsoleData.length() == 1;
    }

    private void loadData() {
        flightsController.loadData();
    }
}
