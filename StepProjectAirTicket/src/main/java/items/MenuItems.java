package items;

public enum MenuItems {
    ITEMS("Test"),
    ONLINE_BOARD("1-Online-Board"),
    FLIGHT_INFO("2-Show flight info"),
    BOOKING("3-Search and book a flight"),
    CANCEL_BOOKING("4-Cancel the booking"),
    MY_FLIGHTS("5-My flights"),
    EXIT("6-Exit");
    final String text;

    MenuItems(String s) {
        this.text = s;
    }

    public String getText() {
        return text;
    }
    public void showMenuItems(){
        System.out.println("-----------------");
        System.out.println(ONLINE_BOARD.text);
        System.out.println(FLIGHT_INFO.text);
        System.out.println(BOOKING.text);
        System.out.println(CANCEL_BOOKING.text);
        System.out.println(MY_FLIGHTS.text);
        System.out.println(EXIT.text);
        System.out.println("-----------------");
    }
}
