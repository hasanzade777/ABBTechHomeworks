package servicesDAO;

import model.Bookings;
import model.Flights;

import java.util.List;

public interface FileServiceDAO {
    void loadDataFlights();

    void loadDataBookings();

    void saveBookings(List<Bookings> bookingsList);

    void saveFlights(List<Flights> flightsList);
}
