package servicesDAO;

import model.Bookings;

import java.util.List;

public interface BookingsDAOService {
    Bookings getByNameSurname(String name, String surname);

    void save(List<Bookings> listOfBookings);

    void addBooking(Bookings booking);

    void removeBooking(int ID);

    List<Bookings> getAllBookings();

    Bookings getBookingsForID(int ID);
}
