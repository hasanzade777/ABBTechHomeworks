package servicesDAO;

import model.Bookings;
import model.Flights;

import java.util.List;

public interface FlightsDAOService {
    Flights getBySerialNumberID(String ID);

    List<Flights> getAllFlights();

    void save(List<Flights> flights);

    void addBookingToFlight(Bookings booking);

    void removeBookingFromFlight(Bookings booking);
}
