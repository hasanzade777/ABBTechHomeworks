package servicesDAO.impl;

import exception.FlightsExceptionClass;
import model.Bookings;
import model.Flights;
import model.Passengers;
import servicesDAO.BookingsDAOService;
import servicesDAO.FileServiceDAO;
import servicesDAO.FlightsDAOService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FileServiceImpl implements FileServiceDAO {
    public FileServiceImpl() {
    }

    private final FlightsDAOService flightsDAOService = new FlightsDaoServiceImpl();

    private final BookingsDAOService bookingsDAOService = new BookingsDaoServiceImpl();
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");

    @Override
    public void loadDataFlights() {
        try {

            FileReader fileReader = new FileReader("FlightDatabase.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            var flightsList = bufferedReader.lines().filter(l -> !l.isBlank()).collect(Collectors.toList());
            bufferedReader.close();
            Flights flight = new Flights();
            List<Flights> flights = new ArrayList<>();
            for (String line : flightsList) {
                List<String> list = List.of(line.split(","));
                flights.add(new Flights(Integer.parseInt(list.get(0).trim()), list.get(1).trim(), list.get(2).trim(), list.get(3).trim(),
                        LocalDateTime.parse(list.get(4).trim(), dateTimeFormatter), Integer.parseInt(list.get(5).trim())));
            }
            flightsDAOService.save(flights);
        } catch (Exception e) {
            new FlightsExceptionClass("Load data from FlightsDatabase Unsuccessfully");
        }
    }

    @Override
    public void loadDataBookings() {
        try {
            FileReader fileReader = new FileReader("Bookings.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            Bookings bookings = new Bookings();
            List<Bookings> listOfBookings = new ArrayList<>();
            var bookingsList = bufferedReader.lines().filter(l -> !l.isBlank()).collect(Collectors.toList());
            bufferedReader.close();
            for (String line : bookingsList) {
                List<String> list = List.of(line.split(","));
                listOfBookings.add(new Bookings(Integer.parseInt(list.get(0).trim()), new Passengers(list.get(1).trim(), list.get(2).trim()), list.get(3).trim()));
            }
            bookingsDAOService.save(listOfBookings);
        } catch (Exception e) {
            new FlightsExceptionClass("Load data bookings have thrown exception");
        }

    }

    @Override
    public void saveBookings(List<Bookings> bookingsList) {
        try {
            FileWriter fileWriter = new FileWriter("Bookings.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (Bookings bookings : bookingsList) {
                bufferedWriter.write(bookings.toString());
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception e) {
            new FlightsExceptionClass("The bookingsList save exception thrown");
        }
    }

    @Override
    public void saveFlights(List<Flights> flightsList) {
        try {
            FileWriter fileWriter = new FileWriter("FlightDatabase.txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (Flights flights : flightsList) {
                bufferedWriter.write(flights.toString());
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (Exception e) {
            new FlightsExceptionClass("The flightsList save exception thrown");
        }
    }
}
