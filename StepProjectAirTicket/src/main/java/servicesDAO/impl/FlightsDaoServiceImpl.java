package servicesDAO.impl;

import model.Bookings;
import model.Flights;
import servicesDAO.FlightsDAOService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FlightsDaoServiceImpl implements FlightsDAOService {
    private static List<Flights> allFlights = new ArrayList<>();

    public FlightsDaoServiceImpl() {
    }

    @Override
    public Flights getBySerialNumberID(String flightSerialID) {
        return allFlights.stream().filter(x -> x.getFlightSerialNumber().equals(flightSerialID)).findFirst().orElse(new Flights());
    }

    @Override
    public List<Flights> getAllFlights() {
        return allFlights;
    }

    @Override
    public void save(List<Flights> flights) {
        allFlights.addAll(flights);
    }

    @Override
    public void addBookingToFlight(Bookings booking) {
        List<Bookings> bookings = new ArrayList<>();
        var bookingsList = allFlights.stream().filter(v -> v.getFlightSerialNumber().equals(booking.getFlightID())).collect(Collectors.toList());
        if (bookingsList.size() > 0 && bookingsList.get(0).getBookings() != null) {
            bookings = bookingsList.stream().flatMap(
                    v -> v.getBookings().stream()
            ).collect(Collectors.toList());
        }
        bookings.add(booking);
        List<Bookings> finalBookings = bookings;
        allFlights.stream().filter(x -> x.getFlightSerialNumber().equals(booking.getFlightID())).forEach(v -> {
            v.setBookings(finalBookings);
            v.setSeats(v.getSeats() - bookingsList.size());
        });

    }

    @Override
    public void removeBookingFromFlight(Bookings booking) {
        allFlights.stream().filter(x -> x.getFlightSerialNumber().equals(booking.getFlightID())).forEach(v -> {
            v.getBookings().remove(booking);
            v.setSeats(v.getSeats() + 1);
        });
    }
}
