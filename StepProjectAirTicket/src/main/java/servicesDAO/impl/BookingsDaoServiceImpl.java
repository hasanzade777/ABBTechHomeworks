package servicesDAO.impl;

import model.Bookings;
import servicesDAO.BookingsDAOService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookingsDaoServiceImpl implements BookingsDAOService {
    public BookingsDaoServiceImpl() {
    }

    private static List<Bookings> bookingsList = new ArrayList<>();

    @Override
    public Bookings getByNameSurname(String name, String surname) {
        return bookingsList.stream().filter(x ->
                x.getPassenger().getName().equals(name) &&
                        x.getPassenger().getSurname().equals(surname)
        ).findFirst().orElse(new Bookings());
    }

    @Override
    public void save(List<Bookings> listOfBookings) {
        bookingsList.addAll(listOfBookings);
    }

    @Override
    public void addBooking(Bookings booking) {
        bookingsList.add(booking);
    }

    @Override
    public void removeBooking(int ID) {
        bookingsList.removeIf(x -> x.getID() == ID);
    }

    @Override
    public List<Bookings> getAllBookings() {
        return bookingsList;
    }

    @Override
    public Bookings getBookingsForID(int ID) {
        return bookingsList.stream().filter(x -> x.getID() == ID).findFirst().orElse(new Bookings());
    }
}
