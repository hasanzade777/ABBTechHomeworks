package controller;

import model.Bookings;
import model.Flights;
import service.BookingsService;
import service.FlightsService;
import service.impl.BookingsServiceImpl;
import service.impl.FlightsServiceImpl;

import java.time.LocalDateTime;
import java.util.List;

import static servicesDAO.impl.FileServiceImpl.dateTimeFormatter;

public class FlightsController {
    private final BookingsService bookingsService = new BookingsServiceImpl();
    private final FlightsService flightsService = new FlightsServiceImpl();


    public void showOnlineBoardPrettyFormat() {
        flightsService.showOnlineBoardPrettyFormat();
    }

    public Flights showOneFlight(String serialID) {
        return flightsService.showOneFlight(serialID);
    }

    public boolean searchTheFlight(String destination, String dateTime, int tickets) {
        return bookingsService.searchTheFlight(destination, LocalDateTime.parse(dateTime, dateTimeFormatter), tickets);
    }

    public void bookTheFlight(String flightSerialID, String name, String surname) {
        bookingsService.bookTheFlight(flightSerialID, name, surname);
    }

    public boolean cancelTheBooking(int ID) {
        return bookingsService.cancelTheBooking(ID);
    }

    public List<Bookings> myFlights(String name, String surname) {
        var bookings = bookingsService.myFlights(name, surname);
        if (bookings.isEmpty()) {
            System.out.println("There is no information or bookings for this passenger");
        }
        return bookings;
    }

    public void loadData() {
        flightsService.loadDataToApp();
        bookingsService.loadDataToApp();
    }

    public void addAllDataToFile() {
        flightsService.saveAllFlights();
        bookingsService.saveAllBookings();
    }
}
