package service;

import model.Bookings;
import model.Flights;

import java.util.List;

public interface FlightsService {
    List<Flights> getAllFlights();

    Flights showOneFlight(String ID);

    void loadDataToApp();

    void addBookingToFlight(Bookings booking);

    void removeBooking(Bookings booking);

    void saveAllFlights();

    void showOnlineBoardPrettyFormat();
}
