package service.impl;

import model.Bookings;
import model.Flights;
import service.FlightsService;
import servicesDAO.FileServiceDAO;
import servicesDAO.FlightsDAOService;
import servicesDAO.impl.FileServiceImpl;
import servicesDAO.impl.FlightsDaoServiceImpl;

import java.util.List;

public class FlightsServiceImpl implements FlightsService {
    private final FlightsDAOService flightsDAOService = new FlightsDaoServiceImpl();
    private final FileServiceDAO fileService = new FileServiceImpl();

    @Override
    public List<Flights> getAllFlights() {
        return flightsDAOService.getAllFlights();
    }

    @Override
    public void showOnlineBoardPrettyFormat() {
        var flightsList = getAllFlights();
        for (Flights flight : flightsList) {
            System.out.println(flight.prettyFormat());
        }
    }

    @Override
    public Flights showOneFlight(String ID) {
        return flightsDAOService.getBySerialNumberID(ID);
    }

    @Override
    public void loadDataToApp() {
        fileService.loadDataFlights();
    }

    @Override
    public void addBookingToFlight(Bookings booking) {
        flightsDAOService.addBookingToFlight(booking);
    }

    @Override
    public void removeBooking(Bookings booking) {
        flightsDAOService.removeBookingFromFlight(booking);
    }

    @Override
    public void saveAllFlights() {
        fileService.saveFlights(getAllFlights());
    }
}
