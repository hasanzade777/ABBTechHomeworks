package service.impl;

import model.Bookings;
import model.Passengers;
import service.BookingsService;
import service.FlightsService;
import servicesDAO.FileServiceDAO;
import servicesDAO.impl.BookingsDaoServiceImpl;
import servicesDAO.impl.FileServiceImpl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BookingsServiceImpl implements BookingsService {
    private final FlightsService flightsService = new FlightsServiceImpl();
    private final BookingsDaoServiceImpl bookingsService = new BookingsDaoServiceImpl();
    private final FileServiceDAO fileService = new FileServiceImpl();


    @Override
    public boolean searchTheFlight(String destination, LocalDateTime date, int ticketsCount) {
        var flights = flightsService.getAllFlights();
        flights = flights.stream().filter(
                x -> Objects.equals(x.getDestination(), destination) && Objects.equals(x.getFlightDateTime(), date) &&
                        x.getSeats() > ticketsCount
        ).collect(Collectors.toList());
        if (flights.size() > 0) {
            flights.forEach(System.out::println);
            return true;
        } else return false;
    }

    @Override
    public void bookTheFlight(String serialNumberOfFlight, String name, String surname) {
        var booking = new Bookings(lastID(), new Passengers(name, surname), serialNumberOfFlight);
        bookingsService.addBooking(booking);
        flightsService.addBookingToFlight(booking);
    }

    @Override
    public boolean cancelTheBooking(int ID) {
        var allBookings = bookingsService.getAllBookings();
        if ((allBookings.size() > 0 && ID > allBookings.get(allBookings.size() - 1).getID()) || allBookings.isEmpty()) {
            System.out.println("The ID not exist");
            return false;
        } else {
            var booking = bookingsService.getBookingsForID(ID);
            flightsService.removeBooking(booking);
            bookingsService.removeBooking(ID);
            return true;
        }
    }

    @Override
    public List<Bookings> myFlights(String name, String surname) {
        return bookingsService.getAllBookings().stream().filter(
                x -> x.getPassenger().getName().equalsIgnoreCase(name) && x.getPassenger().getSurname().equalsIgnoreCase(surname)
        ).collect(Collectors.toList());
    }

    @Override
    public void loadDataToApp() {
        fileService.loadDataBookings();
    }

    @Override
    public void saveAllBookings() {
        fileService.saveBookings(bookingsService.getAllBookings());
    }

    private int lastID() {
        var books = bookingsService.getAllBookings();
        return books.size() == 0 ? 1 : books.get(books.size() - 1).getID() + 1;
    }

}
