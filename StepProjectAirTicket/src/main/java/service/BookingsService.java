package service;

import model.Bookings;

import java.time.LocalDateTime;
import java.util.List;

public interface BookingsService {
    boolean searchTheFlight(String destination, LocalDateTime date, int ticketsCount);

    void bookTheFlight(String serialNumberOfFlight, String name, String surname);

    boolean cancelTheBooking(int ID);

    List<Bookings> myFlights(String name, String Surname);

    void loadDataToApp();

    void saveAllBookings();

}
