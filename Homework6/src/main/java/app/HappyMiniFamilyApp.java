package app;

import service.Family;
import service.Human;
import service.Pet;
import service.Species;

public class HappyMiniFamilyApp {

    public static void main(String[] args) {
        Human human = new Human();
        Human caroline = new Human("Caroline", "Doe", 1955, 76, human.enterSchedule());
        Human john = new Human("John", "Doe", 1944, 77, human.enterSchedule());
        Human michael = new Human("Michael", "Doe", 1995, 85, human.enterSchedule());
        Human dolares = new Human("Dolares", "Doe", 1996, 84, human.enterSchedule());
        Human volki = new Human("Volki","Dolki",1988);
        Pet sugar = new Pet(Species.CAT, "Sugar", 2,77, "eat", "sleep", "myau");
        Family familyOfJohn = new Family(caroline, john, sugar, dolares, michael);
        System.out.println(familyOfJohn);
        System.out.println("call method for return boolean "+familyOfJohn.deleteChild(0));
        System.out.println("call method for return boolean "+familyOfJohn.deleteChild(5));
        System.out.println("After+delete->"+familyOfJohn);
        familyOfJohn.addChild(michael);
        System.out.println("After+adding->"+familyOfJohn);
        System.out.println("call method for return boolean obj "+familyOfJohn.deleteChild(michael));
        System.out.println("call method for return boolean obj "+familyOfJohn.deleteChild(volki));
        System.out.println("After+delete->"+familyOfJohn);
        Family fam = new Family();
        Pet pe = new Pet() ;
        Human hw1 = new Human();
        pe = null;
        fam = null ;
        hw1 = null ;
        System.gc();
        System.out.println(sugar);
        System.out.println("Family members " + familyOfJohn.countFamily());
    }
}
