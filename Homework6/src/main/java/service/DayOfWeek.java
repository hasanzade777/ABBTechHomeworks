package service;

public enum DayOfWeek {
    MONDAY,
    THURSDAY,
    WEDNESDAY,
    TUESDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}
