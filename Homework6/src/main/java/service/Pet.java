package service;

import java.util.Arrays;

public class Pet implements PetService {
    private Species species;
    private String nickname;
    private int age;
    private String[] habits;
    private int trickLevel;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String... habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.habits = habits;
        this.trickLevel = trickLevel;
    }

    @Override
    public String eat() {
        return "I'm eating";
    }

    @Override
    public String respond() {
        return "Hello, owner. I am - " + nickname + ". I miss you!";
    }

    @Override
    public String foul() {
        return "I need to cover it up";
    }

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                ", extra=" + species.getInfo() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet pet = (Pet) o;

        if (!species.equals(pet.species)) return false;
        if (!nickname.equals(pet.nickname)) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(habits, pet.habits);
    }

    @Override
    public int hashCode() {
        int result = species.hashCode();
        result = 22 * result + (nickname != null ? nickname.hashCode() : 0);
        result = 22 * result + age;
        result = 22 * result + trickLevel;
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("The finalize of Pet called");
    }
}
