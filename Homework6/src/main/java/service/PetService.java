package service;

public interface PetService {
    String eat();
    String respond();
    String foul();
}
