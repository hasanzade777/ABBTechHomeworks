import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import service.Pet;
import service.Species;

public class PetTest {
    Pet sugar = new Pet(Species.CAT, "Sugar", 2,77, "eat", "sleep", "myau");
    @Test
    public void eatTest(){
        String expected = "I'm eating";
        assertEquals(expected,sugar.eat());
    }
    @Test
    public void respondTest(){
        String expected = "Hello, owner. I am - " + sugar.getNickname() + ". I miss you!";
        assertEquals(expected,sugar.respond());
    }
    @Test
    public void foulTest(){
        String expected = "I need to cover it up";
        assertEquals(expected,sugar.foul());
    }
}
