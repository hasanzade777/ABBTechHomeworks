import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import service.Family;
import service.Human;
import service.Pet;
import service.Species;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {
    Human human = new Human();
    Human caroline = new Human("Caroline", "Doe", 1955, 76, human.enterSchedule());
    Human john = new Human("John", "Doe", 1944, 77, human.enterSchedule());
    Human michael = new Human("Michael", "Doe", 1995, 85, human.enterSchedule());
    Human dolares = new Human("Dolares", "Doe", 1996, 84, human.enterSchedule());
    Human volki = new Human("Volki", "Dolki", 1988);
    Pet sugar = new Pet(Species.CAT, "Sugar", 2, 77, "eat", "sleep", "myau");
    Family familyOfJohn = new Family(caroline, john, sugar, dolares, michael);

    @Test
    public void DescribePetTest() {
        String expected = "I have CAT,he is 2 years old,he very sly";
        assertEquals(expected, caroline.describePet());
    }

    @Test
    public void GreetTest() {
        String expected = "Hello, Sugar";
        assertEquals(expected, john.greetPet());
    }

    @Test
    public void feedPet() {
        String expected = "You have no pet";
        String expected2 = "Hm... I will feed " + michael.getFamily().getPet().getNickname();
        assertEquals(expected,human.feedPet(true));
        assertEquals(expected2,michael.feedPet(true));
    }

    @Test
    public void humanEqualsTest() {
        Human vally = new Human("Vally", "Vallier", 1995);
        Human vally2 = new Human("Vally", "Vallier", 1995);
        Human terry = new Human("Vally", "Howard", 1995);
        boolean assertEqualsTrue = vally2.equals(vally);
        boolean assertEqualsFalse = vally.equals(terry);
        assertTrue(assertEqualsTrue);
        assertFalse(assertEqualsFalse);
    }

    @Test
    public void humanHashcodeTest() {
        Human vally = new Human("Vally", "Vallier", 1995);
        Human vally2 = new Human("Vally", "Vallier", 1995);
        Human terry = new Human("Vally", "Howard", 1995);
        boolean assertEqualsTrue = vally2.hashCode() == vally.hashCode();
        boolean assertEqualsFalse = vally.hashCode() == terry.hashCode();
        assertTrue(assertEqualsTrue);
        assertFalse(assertEqualsFalse);
    }
}
