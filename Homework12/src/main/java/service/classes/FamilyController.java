package service.classes;

import Exceptions.FamilyOverFlownException;
import model.Family;
import model.Human;
import model.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    public FamilyService familyService = new FamilyService();

    public List<String> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public List<Family> displayAllFamilies() {
        return familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int biggerThan) {
        try {
            return familyService.getFamiliesBiggerThan(biggerThan);
        } catch (FamilyOverFlownException e) {
            throw new FamilyOverFlownException("Invalid number");
        }
    }

    public List<Family> getFamiliesLessThan(int lesserThan) {
        try {
            return familyService.getFamiliesLessThan(lesserThan);
        } catch (FamilyOverFlownException e) {
            throw new FamilyOverFlownException("Invalid number");
        }
    }

    public Integer countFamiliesWithMemberNumber(Family family) {
        return familyService.countFamiliesWithMemberNumber(family);
    }

    public void createNewFamily(Human father, Human mother) {
        familyService.createNewFamily(father, mother);
    }

    public boolean deleteFamilyByIndex(int familyIndex) {
        return familyService.deleteFamilyByIndex(familyIndex);
    }

    public Family bornChild(Family family, String type) {
        try {
            return familyService.bornChild(family, type);
        } catch (FamilyOverFlownException e) {
            throw new FamilyOverFlownException("Invalid type or family doesn't exist");
        }
    }

    public Family adoptTheChild(Family family, Human child) {
        try {
            return familyService.adoptTheChild(family, child);
        } catch (FamilyOverFlownException e) {
            throw new FamilyOverFlownException("Invalid type or family doesn't exist");
        }
    }

    public void deleteAllChildrenOlderThen(int olderThen) {
        familyService.deleteAllChildrenOlderThen(olderThen);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int familyIndex) {
        return familyService.getFamilyById(familyIndex);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }
}
