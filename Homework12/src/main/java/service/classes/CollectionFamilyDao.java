package service.classes;

import model.*;
import service.interfaces.FamilyDao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao {
    Human human = new Human();
    Dog dog = new Dog("Rex", 12, 88, "Eat", "Grr", "Walk");
    Fish fish = new Fish("Bloop", 1, 11, "Swim", "Nothing much");
    DomesticCat domesticCat = new DomesticCat("Mesi", 5, 99, "Eat", "Play", "Joy");
    RoboCat roboCat = new RoboCat("Robot", 3, 0, "Fast", "Charging", "");
    Man John = new Man("John", "Doe", LocalDate.of(1987, 12, 4), 102, human.enterSchedule());
    Woman Sue = new Woman("Sue", "Doe", LocalDate.of(1998, 4, 23));
    Woman Mia = new Woman("Mia", "Doe", LocalDate.of(2003, 6, 30));
    Sheep sheep = new Sheep("Qoyun", 3, 12, "eat", "give milk");
    Family familyOfJohn = new Family(Sue, John, Set.of(domesticCat), Mia);
    Man James = new Man("James", "Vornir", LocalDate.of(1897, 6, 21), 102, human.enterSchedule());
    Woman Emma = new Woman("Emma", "Vornir", LocalDate.of(1893, 4, 12), 100, human.enterSchedule());
    Woman Abigail = new Woman("Abigail", "Vornir", LocalDate.of(1930, 1, 4), 100, human.enterSchedule());
    Man Anthony = new Man("Anthony", "Vornir", LocalDate.of(1934, 2, 23), 102, human.enterSchedule());
    Family familyOfJames = new Family(Emma, James, Set.of(roboCat), Abigail, Anthony);
    Man Robert = new Man("Robert", "Derive", LocalDate.of(1930, 3, 25), 97, human.enterSchedule());
    Woman Luna = new Woman("Luna", "Derive", LocalDate.of(1744, 2, 4), 100, human.enterSchedule());
    Family familyOfRobert = new Family(Luna, Robert, Set.of(fish));
    Man David = new Man("David", "Warner", LocalDate.of(1998, 7, 11), 102, human.enterSchedule());
    Woman Camila = new Woman("Camila", "Warner", LocalDate.of(2004, 9, 15), 100, human.enterSchedule());
    Woman Isabella = new Woman("Isabella", "Warner", LocalDate.of(2003, 8, 7), 100, human.enterSchedule());
    Woman Avery = new Woman("Avery", "Warner", LocalDate.of(2004, 5, 8), 100, human.enterSchedule());
    Woman Aria = new Woman("Aria", "Warner", LocalDate.of(2004, 6, 9), 100, human.enterSchedule());
    Family familyOfDavid = new Family(Camila, David, Set.of(sheep, dog), Isabella, Avery, Aria);
    Man Daryl = new Man("Daryl", "Darl", LocalDate.of(1998, 7, 11), 102, human.enterSchedule());
    Woman Ulru = new Woman("Ulru", "Darl", LocalDate.of(2004, 9, 15), 100, human.enterSchedule());
    Family familyOfDaryl = new Family(Ulru, Daryl);
    private List<Family> familyList = new ArrayList<>(Arrays.asList(familyOfDavid, familyOfJames, familyOfRobert, familyOfJohn, familyOfDaryl));


    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }

    @Override
    public Family getFamilyByIndex(int i) {
        return familyList.size() > i ? familyList.get(i) : null;
    }

    @Override
    public boolean deleteFamily(int i) {
        if (familyList.size() > i) {
            familyList.remove(i);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        int familySize = familyList.size();
        familyList.forEach(x -> {
            if (family.equals(x)) {
                familyList.remove(x);
            }
        });
        return familyList.size() != familySize;
    }

    @Override
    public void saveFamily(Family family) {
        final int[] i = {0};
        familyList.forEach(x -> {
            if (x.equals(family)) {
                i[0] = 1;
                x.setMother(family.getMother());
                x.setFather(family.getFather());
                x.setChildren(family.getChildren());
                x.setPet(family.getPet());
            }
        });
        if (i[0] == 0) familyList.add(family);
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int biggerThan) {
        return familyList.stream()
                .filter(x -> x.countFamily() > biggerThan).collect(Collectors.toList());
    }

    @Override
    public List<Family> getFamiliesLessThan(int lesserThan) {
        return familyList.stream()
                .filter(x -> x.countFamily() < lesserThan).collect(Collectors.toList());
    }

    @Override
    public Family getFamily(Family family) {
        return familyList.stream().filter(x -> x.equals(family)).findFirst().get();
    }

}
