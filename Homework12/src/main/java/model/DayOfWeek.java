package model;

public enum DayOfWeek {
    MONDAY,
    THURSDAY,
    WEDNESDAY,
    TUESDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}
