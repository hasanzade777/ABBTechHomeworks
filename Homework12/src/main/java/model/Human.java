package model;

import service.interfaces.HumanIntrfc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Stream;

public class Human implements HumanIntrfc {
    private String name;
    private String surname;
    private LocalDate birthDate;
    private int IQ;
    private Map<String, String> schedule = new LinkedHashMap<>();
    private Family family;
    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public Human(String name, String surname, LocalDate birthDate, int IQ) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.IQ = IQ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public int getIQ() {
        return IQ;
    }

    public void setIQ(int IQ) {
        this.IQ = IQ;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Human() {

    }

    public Human(String name, String surname, LocalDate birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, LocalDate birthDate, int IQ, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.IQ = IQ;
        this.schedule = schedule;
    }

    public Human(String name, String surname, LocalDate birthDate, int IQ, Map<String, String> schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.IQ = IQ;
        this.schedule = schedule;
        this.family = family;
    }

    @Override
    public String toString() {
        String date = birthDate.format(dateFormatter);
        return
                "name='" + name + '\'' +
                        ", surname='" + surname + '\'' +
                        ", birthDate=" + date +
                        ", IQ=" + IQ +
                        ", schedule=" + schedule;
    }

    public String prettyFormat() {
        String date = birthDate.format(dateFormatter);
        String gender;
        if (this.getClass() == Man.class) gender = "boy";
        else gender = "girl";
        return "            " +
                gender + ": "
                + "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + date +
                ", IQ=" + IQ +
                ", schedule=" + schedule +
                "\n";
    }

    public Map<String, String> enterSchedule() {
        String[][] schedule1 = new String[7][2];
        schedule1[0][0] = DayOfWeek.MONDAY.name();
        schedule1[1][0] = DayOfWeek.TUESDAY.name();
        schedule1[0][1] = "go to cinema";
        schedule1[1][1] = "go to school";
        Stream.of(schedule1).filter(Objects::nonNull)
                .forEach(x -> schedule.put(x[0], x[1]));
        return schedule;
    }

    public String greetPet() {
        return ("Hello, " + family.getPet().iterator().next().getNickname());
    }

    @Override
    public String describePet() {
        String slyCheck = family.getPet().iterator().next().getTrickLevel() > 50 ? "very sly" : "almost not sly";
        return ("I have " + family.getPet().iterator().next().getSpecies().name() + ",he is "
                + family.getPet().iterator().next().getAge() + " years old," + "he " + slyCheck);
    }

    @Override
    public String feedPet(boolean isItTimeForFeeding) {
        Random random = new Random();
        int generatedNumber = random.nextInt(100) + 1;
        if (isItTimeForFeeding || generatedNumber < family.getPet().iterator().next().getTrickLevel()) {
            return ("Hm... I will feed " + family.getPet().iterator().next().getNickname());
        } else {
            return ("I think " + family.getPet().iterator().next().getNickname() + " is not hungry.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (birthDate != human.birthDate) return false;
        if (IQ != human.IQ) return false;
        if (!name.equals(human.name)) return false;
        if (!surname.equals(human.surname)) return false;
        return Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 7 * result + name.hashCode();
        result = 7 * result + surname.hashCode();
        result = 7 * result + birthDate.hashCode();
        result = 7 * result + IQ;
        result = 7 * result + (family != null ? 1 : 0);
        return result;
    }
}
