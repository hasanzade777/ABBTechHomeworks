package app;

import model.Family;
import model.Man;
import model.Woman;
import service.classes.FamilyController;

import java.time.LocalDate;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Console {
    String input;
    Scanner consoleInput = new Scanner(System.in);
    FamilyController familyController = new FamilyController();

    public void run() {
        do {
            System.out.println("-----------------");
            System.out.println("- 1. Display the entire list of families");
            System.out.println("- 2. Display a list of families where the number of people is greater than the specified number");
            System.out.println("- 3. Display a list of families where the number of people is less than the specified number");
            System.out.println("- 4. Calculate the number of families where the number of members is");
            System.out.println("- 5. Create a new family");
            System.out.println("- 6. Delete a family by its index in the general list");
            System.out.println("- 7. Edit a family by its index in the general list");
            System.out.println("- 8. Remove all children over the age of majority");
            System.out.println("-----------------");
            input = consoleInput.next();
            inputAnswer(input);
        } while (!input.equals("exit"));
    }

    public void inputAnswer(String input) {
        int check;
        if (!inputCheckStart(input)) System.out.println("Please enter proper command!(Only Number)");
        else {
            switch (input) {
                case "1":
                    familyController.displayAllFamilies().forEach(Family::prettyFormat);
                    break;

                case "2": {
                    System.out.println("Enter number : ");
                    check = inputCheck(consoleInput.next());
                    familyController.getFamiliesBiggerThan(check).forEach(Family::prettyFormat);
                    break;
                }

                case "3": {
                    System.out.println("Enter number : ");
                    check = inputCheck(consoleInput.next());
                    familyController.getFamiliesLessThan(check).forEach(Family::prettyFormat);
                    break;
                }

                case "4": {
                    System.out.println("Enter number : ");
                    check = inputCheck(consoleInput.next());
                    familyController.displayAllFamilies().stream().filter(family -> (family.countFamily() == check))
                            .forEach(Family::prettyFormat);
                    break;
                }

                case "5":
                    createFamily();
                    break;

                case "6": {
                    System.out.println("Enter ID of family : ");
                    check = inputCheck(consoleInput.next());
                    if (familyController.deleteFamilyByIndex(check)) System.out.println("Deleted");
                    else System.out.println("ID is wrong , returning to main");
                    break;
                }
                case "7": {
                    editFamily();
                    break;
                }
                case "8": {
                    System.out.println("Enter age of children for remove : ");
                    check = consoleInput.nextInt();
                    familyController.deleteAllChildrenOlderThen(check);
                }
            }
        }
    }

    private void editFamily() {
        int response;
        System.out.println("- 1. Give birth to a baby");
        System.out.println("- 2. Adopt a child");
        System.out.println("- 3. Return to main menu");
        response = specialInputChecker(consoleInput.next());
        switch (response) {
            case 1: {
                System.out.println("Please enter ID of family :");
                int id = consoleInput.nextInt();
                Family family = familyController.getFamilyById(id);
                if (family != null) {
                    System.out.println("Please enter masculine for boy,feminine for girl it will be added automatically : ");
                    String boyOrGirl = consoleInput.next();
                    if (boyOrGirl.equals("masculine")) {
                        familyController.bornChild(family, boyOrGirl).prettyFormat();
                    } else if (boyOrGirl.equals("feminine")) {
                        familyController.bornChild(family, boyOrGirl).prettyFormat();
                    } else System.out.println("You have entered none of them returning to main");
                }
                break;
            }
            case 2: {
                System.out.println("Please enter ID of family :");
                int id = consoleInput.nextInt();
                Family family = familyController.getFamilyById(id);
                family.prettyFormat();
                System.out.println("Please enter  first name, last name , year of the birth, IQ : ");
                String name = consoleInput.next();
                String surName = consoleInput.next();
                int yearOfBirth = consoleInput.nextInt();
                int IQ = consoleInput.nextInt();
                if (family.getChildren() != null) {
                    var human = family.getChildren().stream().filter(x ->
                            x.getName().equals(name) && x.getSurname().equals(surName) && x.getBirthDate().getYear() == yearOfBirth
                                    && x.getIQ() == IQ).collect(Collectors.toList());
                    familyController.adoptTheChild(family, human.get(0)).prettyFormat();
                } else System.out.println("This family have no child");
                break;
            }
            case 3:
                break;
        }
    }

    private int specialInputChecker(String input) {
        String str = input;
        if (Integer.parseInt(input) == 1 || Integer.parseInt(input) == 2 || Integer.parseInt(input) == 3)
            return Integer.parseInt(str);
        else {
            System.out.println("Please enter number from range 1-3");
            str = consoleInput.next();
            specialInputChecker(str);
        }
        return Integer.parseInt(str);
    }

    private void createFamily() {
        Woman mother = new Woman();
        Man father = new Man();
        createMother(mother);
        createFather(father);
        familyController.createNewFamily(father, mother);
    }

    private void createMother(Woman mother) {
        int year;
        int month;
        int day;
        System.out.println("Enter mother's name: ");
        mother.setName(consoleInput.next());
        System.out.println("Enter mother's last name: ");
        mother.setSurname(consoleInput.next());
        System.out.println("Enter mother's birth Year: ");
        year = inputCheck(consoleInput.next());
        System.out.println("Enter mother's month of birth: ");
        month = inputCheck(consoleInput.next());
        if (month <= 0 || month > 12) {
            System.out.println("Wrong month");
            month = inputCheck(consoleInput.next());
        }
        System.out.println("Enter mother's day of birth: ");
        day = inputCheck(consoleInput.next());
        if (day <= 0 || day > 30) {
            System.out.println("Wrong day");
            day = inputCheck(consoleInput.next());
        }
        mother.setBirthDate(LocalDate.of(year, month, day));
        System.out.println("Enter mother's IQ: ");
        mother.setIQ(inputCheck(consoleInput.next()));
    }

    private void createFather(Man father) {
        int year;
        int month;
        int day;
        System.out.println("Enter father's name: ");
        father.setName(consoleInput.next());
        System.out.println("Enter father's last name: ");
        father.setSurname(consoleInput.next());
        System.out.println("Enter father's birth Year: ");
        year = inputCheck(consoleInput.next());
        System.out.println("Enter father's month of birth: ");
        month = inputCheck(consoleInput.next());
        if (month <= 0 || month > 12) {
            System.out.println("Wrong month");
            month = inputCheck(consoleInput.next());
        }
        System.out.println("Enter father's day of birth: ");
        day = inputCheck(consoleInput.next());
        if (day <= 0 || day > 30) {
            System.out.println("Wrong day");
            day = inputCheck(consoleInput.next());
        }
        father.setBirthDate(LocalDate.of(year, month, day));
        System.out.println("Enter father's IQ: ");
        father.setIQ(inputCheck(consoleInput.next()));
    }


    private boolean inputCheckStart(String input) {
        return !input.isBlank() && input.matches("[1-9]") && input.length() == 1 || input.equals("exit");
    }

    private int inputCheck(String input) {
        String str = input;
        if (!input.matches("[0-9]+")) {
            System.out.println("Enter number!");
            str = consoleInput.next();
            inputCheck(str);
        }
        return Integer.parseInt(str);
    }
}
