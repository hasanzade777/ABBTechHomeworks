import org.junit.jupiter.api.Test;
import model.DomesticCat;
import model.Family;
import model.Human;

import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {
    Human human = new Human();
    Human caroline = new Human("Caroline", "Doe", LocalDate.of(1995,12,12), 76, human.enterSchedule());
    Human john = new Human("John", "Doe", LocalDate.of(1995,12,12), 77, human.enterSchedule());
    Human michael = new Human("Michael", "Doe", LocalDate.of(1995,12,12), 85, human.enterSchedule());
    Human dolares = new Human("Dolares", "Doe", LocalDate.of(1995,12,12), 84, human.enterSchedule());
    Human volki = new Human("Volki", "Dolki", LocalDate.of(1995,12,12));
    DomesticCat sugar = new DomesticCat("Sugar", 2, 77, "eat", "sleep", "myau");
    Family familyOfJohn = new Family(caroline, john, Set.of(sugar), dolares, michael, volki);

    @Test
    public void countFamilyTest() {
        int expected = 6;
        assertEquals(expected, familyOfJohn.countFamily());
    }

    @Test
    public void deleteFamilyChildIndexTest() {
        boolean assertIndexTrue = familyOfJohn.deleteChild(0);
        boolean assertIndexFalse = familyOfJohn.deleteChild(10);
        assertTrue(assertIndexTrue);
        assertFalse(assertIndexFalse);
    }

    @Test
    public void deleteFamilyChildObjectTest() {
        Human huuman = new Human("Yuri", "Kalayev", LocalDate.of(1995,12,12));
        boolean assertIndexTrue = familyOfJohn.deleteChild(dolares);
        boolean assertIndexFalse = familyOfJohn.deleteChild(huuman);
        assertTrue(assertIndexTrue);
        assertFalse(assertIndexFalse);
    }

    @Test
    public void familyEqualsTest() {
        Family family = new Family(caroline, john, Set.of(sugar), dolares, michael, volki);
        Family family2 = new Family(dolares, john, Set.of(sugar), volki);
        boolean assertEqualsTrue = family.equals(familyOfJohn);
        boolean assertEqualsFalse = family2.equals(familyOfJohn);
        assertTrue(assertEqualsTrue);
        assertFalse(assertEqualsFalse);
    }

    @Test
    public void familyHashcodeTest() {
        Family family = new Family(caroline, john, Set.of(sugar), dolares, michael, volki);
        Family family2 = new Family(dolares, john, Set.of(sugar), volki);
        boolean assertEqualsTrue = family.hashCode() == familyOfJohn.hashCode();
        boolean assertEqualsFalse = family2.hashCode() == familyOfJohn.hashCode();
        assertTrue(assertEqualsTrue);
        assertFalse(assertEqualsFalse);
    }

}
