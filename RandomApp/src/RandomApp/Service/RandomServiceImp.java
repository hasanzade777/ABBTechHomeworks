package RandomApp.Service;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class RandomServiceImp implements RandomService {
    private final Scanner scanner = new Scanner(System.in);
    private final Random random = new Random();
    private String name;
    private final int[] guessedNumbersArray = new int[1000];
    private int i = 0;

    @Override
    public void startGame() {
        System.out.print("Let the game begin ! \n" + "Please enter your name: ");
        name = scanner.next();
        System.out.println("Guess number from range is 0-100");
    }


    private int selectNumber() {
        return random.nextInt(101);
    }

    @Override
    public void guessNumber() {
        int selectedNumber = selectNumber();
        boolean isNumberNotGuessed = true;
        while (isNumberNotGuessed) {
            String guessedNumber = scanner.next();
            if (!guessedNumber.matches("[0-9]+")) {
                System.out.println("Please enter number ! ");
            } else if (Integer.parseInt(guessedNumber) > selectedNumber) {
                System.out.println("Your number is too big. Please, try again.");
                guessedNumbersArray[i++] = Integer.parseInt(guessedNumber);
            } else if (Integer.parseInt(guessedNumber) < selectedNumber) {
                System.out.println("Your number is too small. Please, try again.");
                guessedNumbersArray[i++] = Integer.parseInt(guessedNumber);
            } else if (Integer.parseInt(guessedNumber) == selectedNumber) {
                guessedNumbersArray[i++] = Integer.parseInt(guessedNumber);
                System.out.printf("Congratulations, %s \n", name);
                isNumberNotGuessed = false;
            }
        }
        Arrays.sort(guessedNumbersArray, 0, i);
        System.out.print("Your numbers: ");
        for (int j = 0; j < i; j++)
            System.out.printf("%d ", guessedNumbersArray[j]);
    }
}
