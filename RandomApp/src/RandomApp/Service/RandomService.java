package RandomApp.Service;

public interface RandomService {
    void startGame();
    void guessNumber();
}
