package RandomApp;

import RandomApp.Service.RandomService;
import RandomApp.Service.RandomServiceImp;

public class RandomApp {

    public static void main(String[] args) {
        RandomService randomService = new RandomServiceImp();
        randomService.startGame();
        randomService.guessNumber();
    }
}
