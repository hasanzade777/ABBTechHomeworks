package service.classes;

import model.*;
import service.interfaces.FamilyDao;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyService {
    private final FamilyDao familyDao = new CollectionFamilyDao();

    public List<String> getAllFamilies() {
        return familyDao.getAllFamilies().stream().map(x -> x.getFather().getSurname()
                .concat(" Family")).collect(Collectors.toList());
    }

    public List<Family> displayAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int biggerThan) {
        return familyDao.getFamiliesBiggerThan(biggerThan);
    }

    public List<Family> getFamiliesLessThan(int lesserThan) {
        return familyDao.getFamiliesLessThan(lesserThan);
    }

    public Integer countFamiliesWithMemberNumber(Family family) {
        return family.countFamily();
    }

    public void createNewFamily(Human father, Human mother) {
        familyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int familyIndex) {
        return familyDao.deleteFamily(familyIndex);
    }

    public Family bornChild(Family family, String type) {
        if (Objects.equals(type, "masculine")) {
            Random random = new Random();
            List<String> maleNames = List.of("Joshua", "Kenneth", "Brian", "George", "Ronald");
            var name = maleNames.get(random.nextInt(5));
            Man baby = new Man(name, family.getFather().getSurname(), LocalDate.now().getYear());
            family.addChild(baby);
            familyDao.saveFamily(family);
            return family;
        } else if (Objects.equals(type, "feminine")) {
            Random random = new Random();
            List<String> femaleNames = List.of("Elizabeth", "Eleanor", "Zoey", "Stella", "Victoria");
            var name = femaleNames.get(random.nextInt(5));
            Woman baby = new Woman(name, family.getFather().getSurname(), LocalDate.now().getYear());
            family.addChild(baby);
            familyDao.saveFamily(family);
            return family;
        }
        return null;
    }

    public Family adoptTheChild(Family family, Human child) {
        try {
            var fam = familyDao.getFamily(family);
            fam.deleteChild(child);
            familyDao.saveFamily(fam);
            return familyDao.getFamily(fam);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("No Family");
            return null;
        }
    }

    public void deleteAllChildenOlderThen(int olderThen) {
        int thisYear = LocalDate.now().getYear();
        familyDao.getAllFamilies().forEach(
                family -> family.getChildren().removeIf(
                        child -> (thisYear - child.getYear()) > olderThen
                )
        );
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int familyIndex) {
        try {
            return familyDao.getFamilyByIndex(familyIndex);
        } catch (Exception e) {
            System.out.println("No Family");
            return null;
        }
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyDao.getFamilyByIndex(familyIndex).getPet();
    }

    public void addPet(int familyIndex, Pet pet) {
        var family = familyDao.getFamilyByIndex(familyIndex);
        HashSet<Pet> familyPets = new HashSet<>(family.getPet());
        familyPets.add(pet);
        family.setPet(familyPets);
        familyDao.saveFamily(family);
    }
}
