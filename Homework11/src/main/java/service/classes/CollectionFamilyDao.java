package service.classes;

import model.*;
import service.interfaces.FamilyDao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao {
    Human human = new Human();
    Dog dog = new Dog("Rex", 12, 88, "Eat", "Grr", "Walk");
    Fish fish = new Fish("Bloop", 1, 11, "Swim", "Nothing much");
    DomesticCat domesticCat = new DomesticCat("Mesi", 5, 99, "Eat", "Play", "Joy");
    RoboCat roboCat = new RoboCat("Robot", 3, 0, "Fast", "Charging", "");
    Man John = new Man("John", "Doe", 1987, 102, human.enterSchedule());
    Woman Sue = new Woman("Sue", "Doe", 1998);
    Woman Mia = new Woman("Mia", "Doe", 2003);
    Sheep sheep = new Sheep("Qoyun", 3, 12, "eat", "give milk");
    Family familyOfJohn = new Family(Sue, John, Set.of(domesticCat), Mia);
    Man James = new Man("James", "Vornir", 1898, 102, human.enterSchedule());
    Woman Emma = new Woman("Emma", "Vornir", 1893, 100, human.enterSchedule());
    Woman Abigail = new Woman("Abigail", "Vornir", 1930, 100, human.enterSchedule());
    Man Anthony = new Man("Anthony", "Vornir", 1934, 102, human.enterSchedule());
    Family familyOfJames = new Family(James, Emma, Set.of(roboCat), Abigail, Anthony);
    Man Robert = new Man("Robert", "Derive", 1733, 97, human.enterSchedule());
    Woman Luna = new Woman("Luna", "Derive", 1744, 100, human.enterSchedule());
    Family familyOfRobert = new Family(Robert, Luna, Set.of(fish));
    Man David = new Man("David", "Warner", 1998, 102, human.enterSchedule());
    Woman Camila = new Woman("Camila", "Warner", 2004, 100, human.enterSchedule());
    Woman Isabella = new Woman("Isabella", "Warner", 2003, 100, human.enterSchedule());
    Woman Avery = new Woman("Avery", "Warner", 2004, 100, human.enterSchedule());
    Woman Aria = new Woman("Aria", "Warner", 2004, 100, human.enterSchedule());
    Family familyOfDavid = new Family(David, Camila, Set.of(sheep,dog), Isabella, Avery, Aria);
    private List<Family> familyList = new ArrayList<>(Arrays.asList(familyOfDavid, familyOfJames, familyOfRobert, familyOfJohn));


    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }

    @Override
    public Family getFamilyByIndex(int i) {
        return familyList.size() > i ? familyList.get(i) : null;
    }

    @Override
    public boolean deleteFamily(int i) {
        if (familyList.size() > i) {
            familyList.remove(i);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        int familySize = familyList.size();
        familyList.forEach(x -> {
            if (family.equals(x)) {
                familyList.remove(x);
            }
        });
        return familyList.size() != familySize;
    }

    @Override
    public void saveFamily(Family family) {
        final int[] i = {0};
        familyList.forEach(x -> {
            if (x.equals(family)) {
                i[0] = 1;
                x.setMother(family.getMother());
                x.setFather(family.getFather());
                x.setChildren(family.getChildren());
                x.setPet(family.getPet());
            }
        });
        if (i[0] == 0) familyList.add(family);
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int biggerThan) {
        return familyList.stream()
                .filter(x -> x.countFamily() > biggerThan).collect(Collectors.toList());
    }

    @Override
    public List<Family> getFamiliesLessThan(int lesserThan) {
        return familyList.stream()
                .filter(x -> x.countFamily() < lesserThan).collect(Collectors.toList());
    }

    @Override
    public Family getFamily(Family family) {
        return familyList.stream().filter(x -> x.equals(family)).findFirst().get();
    }

}
