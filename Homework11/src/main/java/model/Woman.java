package model;

import java.util.Map;

public class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int IQ, Map<String, String> schedule) {
        super(name, surname, year, IQ, schedule);
    }

    public Woman(String name, String surname, int year, int IQ, Map<String, String> schedule, Family family) {
        super(name, surname, year, IQ, schedule, family);
    }

    public void makeUp() {
        System.out.println(super.getName() + " I'm beautiful");
    }

    @Override
    public String greetPet() {
        return (super.getFamily().getPet().iterator().next().getSpecies() + " "
                + super.getFamily().getPet().iterator().next().getNickname() + " my pet is sweet");
    }
}
