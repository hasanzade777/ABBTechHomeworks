package model;

public enum Species {
    DOMESTICCAT,
    DOG,
    ROBOCAT,
    FISH,
    UNKNOWN;

}