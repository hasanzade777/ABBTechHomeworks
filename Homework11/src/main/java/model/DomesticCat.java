package model;

import service.interfaces.FoulIntrfc;

import java.util.Set;

public class DomesticCat extends Pet implements FoulIntrfc {
    public DomesticCat() {
    }

    public DomesticCat(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, Set.of(habits));
        this.setSpecies(Species.DOMESTICCAT);
    }

    @Override
    public String respond() {
        return ("Myauu");
    }

    @Override
    public String foul() {
        return ("I need cleaning");
    }
}
