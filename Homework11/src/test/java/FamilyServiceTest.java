import model.Dog;
import model.Family;
import model.Human;
import model.Pet;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import service.classes.FamilyService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    @Mock
    FamilyService familyService = new FamilyService();

    @Test
    void testGetAllFamilies() {
        List<String> actualAllFamilies = (familyService.getAllFamilies());
        assertEquals(4, actualAllFamilies.size());
        assertEquals("Warner Family", actualAllFamilies.get(0));
        assertEquals("Vornir Family", actualAllFamilies.get(1));
        assertEquals("Derive Family", actualAllFamilies.get(2));
        assertEquals("Doe Family", actualAllFamilies.get(3));
    }

    @Test
    void testCountFamiliesWithMemberNumber2() {
        Human mother = new Human("Name", "Doe", 1);

        Human father = new Human("Name", "Doe", 1);

        HashSet<Pet> pet = new HashSet<>();
        assertEquals(4,
                familyService.countFamiliesWithMemberNumber(new Family(mother, father, pet, new Human("Name", "Doe", 1)))
                        .intValue());
    }

    @Test
    void testBornChild() {
        assertNull(familyService.bornChild(new Family(), "Type"));
    }

    @Test
    void testBornChild1() {
        Family family = new Family();
        family.setChildren(new ArrayList<>());
        family.setFather(new Human());
        assertSame(family, familyService.bornChild(family, "feminine"));
    }

    @Test
    void testAdoptTheChild() {
        Human child = familyService.getFamilyById(1).getChildren().get(0);

        Family family = familyService.getFamilyById(1);

        familyService.adoptTheChild(family, child);
    }

    @Test
    void testGetFamilyById() {
        assertNull((familyService.getFamilyById(4)));
    }

    @Test
    void testGetPets() {
        assertEquals(1, (familyService.getPets(1).size()));
    }

    @Test
    void testAddPet() {
        familyService.addPet(1, new Dog());
        assertEquals(4, familyService.getAllFamilies().size());
    }


    @Test
    void testGetCount() {
        assertEquals(4, familyService.count());
    }

    @Test
    void testGetFamiliesBiggerThan() {
        assertEquals(2, familyService.getFamiliesBiggerThan(4).size());
    }

    @Test
    void testGetFamiliesLessThan() {
        assertEquals(1, familyService.getFamiliesLessThan(4).size());
    }

    @Test
    void testDeleteChildrenOlderThen() {
        familyService.deleteAllChildenOlderThen(85);
        assertEquals(4, familyService.getFamilyById(3).countFamily());
    }

    @Test
    void testDisplayAllFamilies() {
        assertEquals(4, familyService.displayAllFamilies().size());
    }

    @Test
    void testDeleteFamilyByIndex() {
        familyService.deleteFamilyByIndex(1);
        assertEquals(3, familyService.displayAllFamilies().size());
    }

    @Test
    void testCreateNewFamily() {
        familyService.createNewFamily(new Human("Eleanor", "Var", 1122), new Human("Vari", "Dara", 2033));
        assertEquals(5, familyService.displayAllFamilies().size());
    }
}

