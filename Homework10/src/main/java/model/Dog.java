package model;

import service.interfaces.FoulIntrfc;

import java.util.Set;

public class Dog extends Pet implements FoulIntrfc {
    public Dog() {

    }

    public Dog(String nickname, int age, int trickLevel, String... habits) {
        super(nickname, age, trickLevel, Set.of(habits));
        this.setSpecies(Species.DOG);
    }

    @Override
    public String respond() {
        return ("hav-hav");
    }

    @Override
    public String foul() {
        return ("Please wash me");
    }
}
