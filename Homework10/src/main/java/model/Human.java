package model;

import service.interfaces.HumanIntrfc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class Human implements HumanIntrfc {
    private String name;
    private String surname;
    private long birthDate;
    private int IQ;
    private Map<String, String> schedule = new LinkedHashMap<>();
    private Family family;

    public Human(String name, String surname, long birthDate, int IQ) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.IQ = IQ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public int getIQ() {
        return IQ;
    }

    public void setIQ(int IQ) {
        this.IQ = IQ;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Human() {

    }

    public Human(String name, String surname, long year) {
        this.name = name;
        this.surname = surname;
        this.birthDate = year;
    }

    public Human(String name, String surname, long year, int IQ, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = year;
        this.IQ = IQ;
        this.schedule = schedule;
    }

    public Human(String name, String surname, long year, int IQ, Map<String, String> schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.birthDate = year;
        this.IQ = IQ;
        this.schedule = schedule;
        this.family = family;
    }

    @Override
    public String toString() {
        Instant instant = Instant.ofEpochSecond(birthDate);
        Date date = Date.from(instant);
        DateFormat dateTimeFormatter = new SimpleDateFormat("dd/MM/yyyy");
        return
                "name='" + name + '\'' +
                        ", surname='" + surname + '\'' +
                        ", year=" + dateTimeFormatter.format(date) +
                        ", IQ=" + IQ +
                        ", schedule=" + schedule +
                        '}';
    }

    public Map<String, String> enterSchedule() {
//        schedule = new HashMap<>(Map.of(DayOfWeek.MONDAY.name(),"go to cinema",
//                DayOfWeek.TUESDAY.name(),"go to school",
//                DayOfWeek.WEDNESDAY.name(),"do homework"));
        String[][] schedule1 = new String[7][2];
        schedule1[0][0] = DayOfWeek.MONDAY.name();
        schedule1[1][0] = DayOfWeek.TUESDAY.name();
        schedule1[2][0] = DayOfWeek.WEDNESDAY.name();
        schedule1[3][0] = DayOfWeek.THURSDAY.name();
        schedule1[4][0] = DayOfWeek.FRIDAY.name();
        schedule1[5][0] = DayOfWeek.SATURDAY.name();
        schedule1[6][0] = DayOfWeek.SUNDAY.name();
        schedule1[0][1] = "go to cinema";
        schedule1[1][1] = "go to school";
        schedule1[2][1] = "do homework";
        schedule1[3][1] = "go to carting";
        schedule1[4][1] = "go out with friends";
        schedule1[5][1] = "go to dinner with family";
        schedule1[6][1] = "go to driving school";
        Stream.of(schedule1).forEach(x -> schedule.put(x[0], x[1]));
        return schedule;
    }

    public String greetPet() {
        return ("Hello, " + family.getPet().iterator().next().getNickname());
    }

    @Override
    public String describePet() {
        String slyCheck = family.getPet().iterator().next().getTrickLevel() > 50 ? "very sly" : "almost not sly";
        return ("I have " + family.getPet().iterator().next().getSpecies().name() + ",he is "
                + family.getPet().iterator().next().getAge() + " years old," + "he " + slyCheck);
    }

    @Override
    public String feedPet(boolean isItTimeForFeeding) {
        Random random = new Random();
        int generatedNumber = random.nextInt(100) + 1;
        if (isItTimeForFeeding || generatedNumber < family.getPet().iterator().next().getTrickLevel()) {
            return ("Hm... I will feed " + family.getPet().iterator().next().getNickname());
        } else {
            return ("I think " + family.getPet().iterator().next().getNickname() + " is not hungry.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (birthDate != human.birthDate) return false;
        if (IQ != human.IQ) return false;
        if (!name.equals(human.name)) return false;
        if (!surname.equals(human.surname)) return false;
        return Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 7 * result + name.hashCode();
        result = 7 * result + surname.hashCode();
        result = (int) (7 * result + birthDate);
        result = 7 * result + IQ;
        result = 7 * result + (family != null ? 1 : 0);
        return result;
    }

    public void describeAge() {
        long birthDate2 = birthDate;
        birthDate2 = System.currentTimeMillis() - birthDate2;
        long day = TimeUnit.SECONDS.toDays(birthDate2);
        long year = day / 365;
        long month = day / 30;
        System.out.println("Year : " + year + " Month : " + month + " Day : " + day);

    }

    public Human(String name, String surname, String birthDate, int IQ) {
        Date date;
        this.name = name;
        this.surname = surname;
        this.IQ = IQ;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date =
                    simpleDateFormat.parse(birthDate);
        } catch (ParseException e) {
            throw new RuntimeException();
        }
        this.birthDate = (date.getTime() / 1000);
    }
}
