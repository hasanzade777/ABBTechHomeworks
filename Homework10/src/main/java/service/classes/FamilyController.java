package service.classes;

import model.Family;
import model.Human;
import model.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    public FamilyService familyService = new FamilyService();

    public List<String> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public List<Family> displayAllFamilies() {
        return familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int biggerThan) {
        return familyService.getFamiliesBiggerThan(biggerThan);
    }

    public List<Family> getFamiliesLessThan(int lesserThan) {
        return familyService.getFamiliesLessThan(lesserThan);
    }

    public Integer countFamiliesWithMemberNumber(Family family) {
        return familyService.countFamiliesWithMemberNumber(family);
    }

    public void createNewFamily(Human father, Human mother) {
        familyService.createNewFamily(father, mother);
    }

    public boolean deleteFamilyByIndex(int familyIndex) {
        return familyService.deleteFamilyByIndex(familyIndex);
    }

    public Family bornChild(Family family, String type) {
        return familyService.bornChild(family, type);
    }

    public Family adoptTheChild(Family family, Human child) {
        return familyService.adoptTheChild(family, child);
    }

    public void deleteAllChildenOlderThen(int olderThen) {
        familyService.deleteAllChildenOlderThen(olderThen);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int familyIndex) {
        return familyService.getFamilyById(familyIndex);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }
}
