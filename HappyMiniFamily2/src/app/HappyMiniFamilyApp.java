package app;

import service.Family;
import service.Human;
import service.Pet;

public class HappyMiniFamilyApp {

    public static void main(String[] args) {
        Human human = new Human();
        Human caroline = new Human("Caroline", "Doe", 1955, 76, human.enterSchedule());
        Human john = new Human("John", "Doe", 1944, 77, human.enterSchedule());
        Human michael = new Human("Michael", "Doe", 1995, 85, human.enterSchedule());
        Human dolares = new Human("Dolares", "Doe", 1996, 84, human.enterSchedule());
        Pet sugar = new Pet("Cat", "Sugar", 2, new String[]{"eat", "sleep", "myau"}, 77);
        Family family1 = new Family(caroline, john, sugar, dolares, michael);
        System.out.println(family1);
        System.out.println("call method for return boolean= " + family1.deleteChild(0));
        System.out.println("After+delete->" + family1);
        System.out.println(michael);
        System.out.println("Family members count : " + family1.countFamily());
        family1.addChild(michael);
        System.out.println("After+adding->" + family1);
        System.out.println("Family members count : " + family1.countFamily());
        System.out.println("call method for return boolean obj= " + family1.deleteChild(michael));
        System.out.println("After+delete->" + family1);
        System.out.println("Family members count : " + family1.countFamily());

        //HumanToString
        System.out.println(michael);
        System.out.println(john);
        //Pet Methods
        sugar.respond();
        sugar.eat();
        sugar.foul();

        //Human Methods
        dolares.greetPet();
        dolares.describePet();
        dolares.feedPet(true);
        dolares.feedPet(false);
        michael.greetPet();
        michael.describePet();
        System.out.println(michael.hashCode());
        System.out.println(dolares.hashCode());
        System.out.println(sugar.hashCode());
        System.out.println(family1.hashCode());
    }
}
