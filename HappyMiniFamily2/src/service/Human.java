package service;

import java.util.Arrays;
import java.util.Random;

public class Human implements HumanService {
    private String name;
    private String surname;
    private int year;
    private int IQ;
    private String[][] schedule = new String[7][2];
    private Family family;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIQ() {
        return IQ;
    }

    public void setIQ(int IQ) {
        this.IQ = IQ;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Human() {

    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int IQ, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.IQ = IQ;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int IQ, String[][] schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.IQ = IQ;
        this.schedule = schedule;
        this.family = family;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", IQ=" + IQ +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    public String[][] enterSchedule() {
        schedule[0][0] = "sunday";
        schedule[1][0] = "monday";
        schedule[2][0] = "tuesday";
        schedule[3][0] = "wednesday";
        schedule[4][0] = "thursday";
        schedule[5][0] = "friday";
        schedule[6][0] = "saturday";
        schedule[0][1] = "go to cinema";
        schedule[1][1] = "go to school";
        schedule[2][1] = "do homework";
        schedule[3][1] = "go to carting";
        schedule[4][1] = "go out with friends";
        schedule[5][1] = "go to dinner with family";
        schedule[6][1] = "go to driving school";
        return schedule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (year != human.year) return false;
        if (IQ != human.IQ) return false;
        if (!name.equals(human.name)) return false;
        if (!surname.equals(human.surname)) return false;
        return family != null ? family.equals(human.family) : human.family == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 24 * result + super.hashCode();
        result = 24 * result + year;
        result = 24 * result + IQ;
        result = 24 * result + (family != null ? super.hashCode() : 0);
        return result;
    }

    public void greetPet() {
        if (family != null) System.out.println("Hello, " + family.getPet().getNickname());
        else System.out.println("You have no pet");
    }

    @Override
    public void describePet() {
        if (family != null) {
            String slyCheck = family.getPet().getTrickLevel() > 50 ? "very sly" : "almost not sly";
            System.out.println("I have " + family.getPet().getSpecies() + ",he is " + family.getPet().getAge() + " years old," + slyCheck);
        } else System.out.println("You have no pet");
    }

    @Override
    public void feedPet(boolean isItTimeForFeeding) {
        if (family != null) {
            Random random = new Random();
            int generatedNumber = random.nextInt(100) + 1;
            if (isItTimeForFeeding || generatedNumber < family.getPet().getTrickLevel()) {
                System.out.println("Hm... I will feed " + family.getPet().getNickname());
            } else {
                System.out.println("I think " + family.getPet().getNickname() + " is not hungry.");
            }
        } else System.out.println("You have no pet");
    }
}
