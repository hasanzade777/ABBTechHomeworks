package service;

public enum Species {
    CAT(false, true, 4),
    DOG(false, true, 4),
    FROG(false, false, 2),
    FISH(false, false, 0),
    SHEEP(false, true, 4);
    private final boolean canFly;
    private final boolean hasFur;
    private final int numberOfLegs;

    Species(boolean canFly, boolean hasFur, int numberOfLegs) {
        this.canFly = canFly;
        this.hasFur = hasFur;
        this.numberOfLegs = numberOfLegs;
    }

    public String getInfo() {
        String result = "Number of legs " + numberOfLegs + ", ";
        if (canFly) result += "This pet can fly, ";
        if (hasFur) result += "This pet has fur ";
        return result;
    }
}