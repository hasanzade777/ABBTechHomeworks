package service;

import java.util.Random;

public class Human implements HumanService {
    private String name;
    private String surname;
    private int year;
    private int IQ;
    private String[][] schedule = new String[7][2];
    private Family family;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIQ() {
        return IQ;
    }

    public void setIQ(int IQ) {
        this.IQ = IQ;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Human() {

    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int IQ, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.IQ = IQ;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year, int IQ, String[][] schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.IQ = IQ;
        this.schedule = schedule;
        this.family = family;
    }

    @Override
    public String toString() {
        return
                "name='" + name + '\'' +
                        ", surname='" + surname + '\'' +
                        ", year=" + year +
                        ", IQ=" + IQ +
                        '}';
    }

    public String[][] enterSchedule() {
        schedule[0][0] = DayOfWeek.MONDAY.name();
        schedule[1][0] = DayOfWeek.TUESDAY.name();
        schedule[2][0] = DayOfWeek.WEDNESDAY.name();
        schedule[3][0] = DayOfWeek.THURSDAY.name();
        schedule[4][0] = DayOfWeek.FRIDAY.name();
        schedule[5][0] = DayOfWeek.SATURDAY.name();
        schedule[6][0] = DayOfWeek.SUNDAY.name();
        schedule[0][1] = "go to cinema";
        schedule[1][1] = "go to school";
        schedule[2][1] = "do homework";
        schedule[3][1] = "go to carting";
        schedule[4][1] = "go out with friends";
        schedule[5][1] = "go to dinner with family";
        schedule[6][1] = "go to driving school";
        return schedule;
    }

    public void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname());
    }

    @Override
    public void describePet() {
        String slyCheck = family.getPet().getTrickLevel() > 50 ? "very sly" : "almost not sly";
        System.out.println("I have " + family.getPet().getSpecies().name() + ",he is "
                + family.getPet().getAge() + " years old," + "he " + slyCheck);
    }

    @Override
    public void feedPet(boolean isItTimeForFeeding) {
        Random random = new Random();
        int generatedNumber = random.nextInt(100) + 1;
        if (isItTimeForFeeding || generatedNumber < family.getPet().getTrickLevel()) {
            System.out.println("Hm... I will feed " + family.getPet().getNickname());
        } else {
            System.out.println("I think " + family.getPet().getNickname() + " is not hungry.");
        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("The finalize of Human called");
    }
}
