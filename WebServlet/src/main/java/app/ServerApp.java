package app;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import servlet.CalculatorHistoryServlet;
import servlet.CalculatorServlet;

public class ServerApp {
    public static void main(String[] args) {
        try {
            Server server = new Server(8080);
            ServletContextHandler servletContextHandler = new ServletContextHandler();
            servletContextHandler.addServlet(CalculatorServlet.class,"/calc");
            servletContextHandler.addServlet(CalculatorHistoryServlet.class,"/calc/history");
            server.setHandler(servletContextHandler);
            server.start();
            server.join();
        } catch (Exception e) {
            System.out.println("Problem has occurred");
        }
    }


}
