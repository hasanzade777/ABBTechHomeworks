package service;

import java.util.List;

public interface CalculatorService {
    Integer calculate(Integer x, Integer y, String op);

    List<String> history();
}
