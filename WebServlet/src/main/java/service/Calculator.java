package service;

import java.util.ArrayList;
import java.util.List;

public class Calculator implements CalculatorService {
    private static final List<String> history = new ArrayList<>();

    public Calculator() {

    }

    @Override
    public Integer calculate(Integer x, Integer y, String operation) {
        history.add(String.valueOf(x));
        history.add(String.valueOf(y));
        switch (operation) {
            case "plus":
                history.add("+");
                history.add(String.valueOf(x + y));
                return x + y;
            case "minus":
                history.add("-");
                history.add(String.valueOf(x - y));
                return x - y;
            case "multiply":
                history.add("*");
                history.add(String.valueOf(x * y));
                return x * y;
            case "divide":
                history.add("/");
                history.add(String.valueOf(x / y));
                return x / y;
            default:
                return 0;
        }
    }

    @Override
    public List<String> history() {
        return history;
    }
}
