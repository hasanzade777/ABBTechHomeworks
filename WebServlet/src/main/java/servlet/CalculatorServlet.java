package servlet;

import service.Calculator;
import service.CalculatorService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CalculatorServlet extends HttpServlet {
    CalculatorService calculatorService = new Calculator();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer firstElement = Integer.valueOf(req.getParameter("x"));
        Integer secondElement = Integer.valueOf(req.getParameter("y"));
        String operation = req.getParameter("op");
        resp.getWriter().println(calculatorService.calculate(firstElement, secondElement, operation));
    }
}
