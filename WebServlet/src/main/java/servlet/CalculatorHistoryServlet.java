package servlet;

import service.Calculator;
import service.CalculatorService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CalculatorHistoryServlet extends HttpServlet {
    CalculatorService calculatorService = new Calculator();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        for (int i = 0; i < calculatorService.history().size() - 3; i = i + 4) {
            resp.getWriter().printf("%1$s %3$s %2$s = %4$s\n", calculatorService.history().get(i), calculatorService.history().get(i + 1),
                    calculatorService.history().get(i + 2), calculatorService.history().get(i + 3));
        }
    }
}
